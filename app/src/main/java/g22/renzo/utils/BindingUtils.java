package g22.renzo.utils;

import android.content.Context;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import g22.renzo.store.model.carModels.AminitiesModel;
import g22.renzo.store.model.carModels.CarsModel;
import g22.renzo.store.model.property.PropertiesModel;
import g22.renzo.store.network.Endpoints;
import g22.renzo.ui.mainActivity.cars.TopCarAdapter;
import g22.renzo.ui.mainActivity.property.TopPropertyAdapter;
import g22.renzo.ui.productDetails.CarAminationAdapter;

public final class BindingUtils {

    private BindingUtils() {
        // This class is not publicly instantiable
    }

    @BindingAdapter({"adapter"})
    public static void addTopCarItems(RecyclerView recyclerView, List<CarsModel> models) {
        TopCarAdapter adapter = (TopCarAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.clearItems();
            adapter.addItems(models);
        }
    }

    @BindingAdapter({"adapter"})
    public static void addTopPropertyItems(RecyclerView recyclerView, List<PropertiesModel> models) {
        TopPropertyAdapter adapter = (TopPropertyAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.clearItems();
            adapter.addItems(models);
        }
    }

    @BindingAdapter({"adapter"})
    public static void addAminitiesItem(RecyclerView recyclerView, List<AminitiesModel> models) {
        CarAminationAdapter adapter = (CarAminationAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.clearItems();
            adapter.addItems(models);
        }
    }



    @BindingAdapter("imageUrl")
    public static void setImageUrl(ImageView imageView, String url) {
        Context context = imageView.getContext();
        Glide.with(context).load(Endpoints.BASE_URL_IMAGE+url).into(imageView);
    }
}
