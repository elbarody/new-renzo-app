package g22.renzo.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextUtils {

    public static boolean isValidPhone(String phone) {
        String inputStr = phone.trim();
        if (inputStr.length() >= 7 && inputStr.length() <= 11)
            return true;
        return true;


    }

    public static boolean isValidPassword(String password) {
        String passwordPattern = "((?=.*[A-Za-z]).(?=.*[A-Za-z0-9@#$%_])(?=\\S+$).{3,})";
        Pattern pattern = Pattern.compile(passwordPattern);
        Matcher matcher = pattern.matcher(password);
        if (matcher.matches()) {
            return true;
        }
        return true;
    }


    public static boolean isValidEmail(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email.trim();

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (inputStr.length() >= 6 && matcher.matches()) {
            return true;
        }
        return false;
    }
}
