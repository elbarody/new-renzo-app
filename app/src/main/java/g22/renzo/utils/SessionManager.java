package g22.renzo.utils;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import g22.renzo.store.model.loginModels.LoginResponse;
import g22.renzo.store.model.loginModels.UserData;

public class SessionManager {

    private static final String PREF_NAME = "app";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String USER_KEY = "user_key";
    private static final String TOKEN_KEY = "token_key";
    private SharedPreferences pref;
    private static SharedPreferences.Editor editor;
    private Context _context;


    public static void createSession(Context context, LoginResponse response) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(USER_KEY, new Gson().toJson(response.getUser()));
        editor.putString(TOKEN_KEY, response.getToken());
        editor.apply();
        editor.commit();
    }

    public static UserData getCurrentUser(Context context) {
        SharedPreferences editor = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        String userJson = editor.getString(USER_KEY, null);
        if (userJson == null)
            return null;
        else
            return new Gson().fromJson(userJson, UserData.class);

    }

    public static void logout(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit();
        editor.remove(USER_KEY);
        editor.remove(TOKEN_KEY);
        editor.apply();
        editor.commit();
    }

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    /**
     * Create login session
     */
    public void createLoginSession(UserData user) {
        Gson gson = new Gson();
        String json = gson.toJson(user);
        editor.putString("user", json);
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to welcomeActivity page
     * Else won't do anything
     */
    public void checkLogin(Class<?> welcomeActivity) {
        // Check login status
        /*if (!this.isLoggedIn()) {
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, welcomeActivity);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // Staring Login Activity
            _context.startActivity(i);
        }*/
    }


    /**
     * Get stored session data
     */
    public UserData getUserDetails() {
        Gson gson = new Gson();
        String json = pref.getString("user", "");
        return gson.fromJson(json, UserData.class);
    }

    public SessionManager getSessionManager() {
        return this;
    }

    /**
     * Clear session details
     */
    public void logoutUser(Class<?> welcomeActivity) {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();


        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, welcomeActivity);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }

    /**
     * Quick check for login
     **/
    // Get Login State
    /*public boolean isLoggedIn() {
        return getUserDetails().getLoginStatus().equals(User.AccountStatus.LOGGED_IN_CLIENT) || getUserDetails().getLoginStatus().equals(User.AccountStatus.LOGGED_IN_PROVIDER);
    }*/
}
