package g22.renzo.utils;


import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.transition.Slide;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.marcinorlowski.fonty.Fonty;

public class DialogBuilder {

    private final View view;
    private OnCounterFinish counterFinish;
    private CountDownTimer countDownTimer;
    private Dialog dialog;

    public DialogBuilder(Context context, @LayoutRes int layoutRes) {
        view = LayoutInflater.from(context).inflate(layoutRes, null);
        dialog = new Dialog(context);
        dialog.setCancelable(true);

        Fonty.setFonts((ViewGroup) view);
        dialog.setContentView(view);
    }

    public DialogBuilder clickListener(@IdRes int viewId, @NonNull OnClickListener onClickListener) {
        View view = this.view.findViewById(viewId);
        view.setVisibility(View.VISIBLE);
        view.setOnClickListener(v -> onClickListener.onClick(dialog, v));
        return this;
    }


    public DialogBuilder disableFloatingHint(@IdRes int inputLayout) {
        TextInputLayout hintView = this.view.findViewById(inputLayout);
        hintView.setHintAnimationEnabled(false);
        hintView.setHint("");
        return this;
    }


    public DialogBuilder setHint(@IdRes int editText, @StringRes int hint) {
        TextInputEditText editTextView = this.view.findViewById(editText);
        editTextView.setHint(view.getContext().getString(hint));
        return this;
    }


    public DialogBuilder editText(@IdRes int viewId, @NonNull OnTextChangeListener onTextChangeListener) {
        EditText view = this.view.findViewById(viewId);
        view.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                onTextChangeListener.onTextChange(editable.toString());
            }
        });
        return this;
    }

    public DialogBuilder rateChangeListener(@IdRes int viewId, @NonNull OnRateChangeListener onRateChangeListener) {
        RatingBar view = this.view.findViewById(viewId);
        view.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> onRateChangeListener.onRateChange(rating));
        return this;
    }

    public DialogBuilder countDownTimer(@IdRes int viewId, long timeOut, long interval) {
        if (viewId == 0) {
            if (timeOut < 0) {
                timeOut = 0;
            }
            if (interval < 0) {
                interval = 0;
            }
            countDownTimer = new CountDownTimer(timeOut, interval) {
                @Override
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {
                    dialog.dismiss();
                }
            };
            dialog.setOnShowListener(dialog1 -> countDownTimer.start());
            dialog.setOnDismissListener(dialog1 -> countDownTimer.cancel());
        } else {
            View view = this.view.findViewById(viewId);
            view.setVisibility(View.VISIBLE);
            if (timeOut < 0) {
                timeOut = 0;
            }
            if (interval < 0) {
                interval = 0;
            }
            countDownTimer = new CountDownTimer(timeOut, interval) {
                @Override
                public void onTick(long millisUntilFinished) {
                    ((TextView) view).setText("00:".concat(String.valueOf(millisUntilFinished / 1000)));
                }

                @Override
                public void onFinish() {
                    dialog.dismiss();
                }
            };
            dialog.setOnShowListener(dialog1 -> countDownTimer.start());
            dialog.setOnDismissListener(dialog1 -> {
                countDownTimer.cancel();
                if (counterFinish != null) {
                    counterFinish.onFinish();
                }
            });
        }
        return this;
    }

    public DialogBuilder background(@DrawableRes int drawableRes) {
        Window window = dialog.getWindow();
        if (window != null) {
            window.getAttributes();
            window.setBackgroundDrawableResource(drawableRes);
        }
        return this;
    }

    public DialogBuilder text(@IdRes int viewId, Spanned text) {
        if (!TextUtils.isEmpty(text)) {
            View view = this.view.findViewById(viewId);
            view.setVisibility(View.VISIBLE);
            ((TextView) view).setText(text);
        } else {
            View view = this.view.findViewById(viewId);
            view.setVisibility(View.GONE);
        }
        return this;
    }

    public DialogBuilder text(@IdRes int viewId, String text) {
        if (!TextUtils.isEmpty(text)) {
            View view = this.view.findViewById(viewId);
            view.setVisibility(View.VISIBLE);
            ((TextView) view).setText(text);
        } else {
            View view = this.view.findViewById(viewId);
            view.setVisibility(View.GONE);
        }
        return this;
    }

    public DialogBuilder gravity(@Slide.GravityFlag int gravity) {
        Window window = dialog.getWindow();
        if (window != null) {
            WindowManager.LayoutParams attributes = window.getAttributes();
            attributes.gravity = gravity;
            attributes.width = WindowManager.LayoutParams.MATCH_PARENT;
            attributes.height = WindowManager.LayoutParams.WRAP_CONTENT;
            //            if ("en". equals(new PreferencesUtil(context).getString(BaseActivity.LOCALE_KEY))) {
//                window.getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
//            } else {
//                window.getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
//            }
            window.setAttributes(attributes);
        }
        return this;
    }

    public DialogBuilder cancelable(boolean cancelable) {
        dialog.setCancelable(cancelable);
        return this;
    }

    public DialogBuilder background(Drawable drawable) {
        Window window = dialog.getWindow();
        if (window != null) {
            dialog.getWindow().setBackgroundDrawable(drawable);
        }
        return this;
    }

    public DialogBuilder counterFinishListener(OnCounterFinish counterFinish) {
        this.counterFinish = counterFinish;
        return this;
    }



    public Dialog build() {
        return dialog;
    }

    public interface OnRateChangeListener {

        void onRateChange(float rate);
    }

    public interface OnClickListener {

        void onClick(Dialog dialog, View view);
    }

    public interface OnCounterFinish {

        void onFinish();
    }

    public interface OnTextChangeListener {

        void onTextChange(String text);
    }

}