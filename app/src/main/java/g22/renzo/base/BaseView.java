package g22.renzo.base;

import dagger.Module;

@Module
public interface BaseView {

    void showErrorMessage(String msg);

    void showSuccessMessage(String msg);

    void showLoading();

    void hideLoading();

    void finishScreen();

}
