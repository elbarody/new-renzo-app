package g22.renzo.base;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.marcinorlowski.fonty.Fonty;

import dagger.android.AndroidInjection;
import g22.renzo.R;
import io.reactivex.disposables.CompositeDisposable;

public abstract class BaseActivity<T extends ViewDataBinding, V extends BaseViewModel> extends AppCompatActivity {

    private KProgressHUD loadingDialog;
    public CompositeDisposable disposable;
    private T mViewDataBinding;
    private V mViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        performDependencyInjection();
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        disposable = new CompositeDisposable();
        /*ImageView backArrow = findViewById(R.id.backBtn);

        if (null != backArrow) {
            backArrow.setOnClickListener(v -> onBackPressed());
        }*/
        onCreateActivityComponent();
        performDataBinding();
        Fonty.setFonts(this);

    }

    public void performDependencyInjection() {
        AndroidInjection.inject(this);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    public abstract int getBindingVariable();

    /**
     * Override for set view homeType
     *
     * @return view homeType instance
     */
    public abstract V getViewModel();

    @LayoutRes
    protected abstract int getLayout();

    protected abstract void onCreateActivityComponent();

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /*@Override
    public void showErrorMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showSuccessMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        if (loadingDialog == null) {
            loadingDialog = KProgressHUD.create(this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCancellable(false)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f)
                    .show();
        } else loadingDialog.show();
    }

    @Override
    public void hideLoading() {
        if (loadingDialog != null)
            loadingDialog.dismiss();
    }

    @Override
    public void finishScreen() {
        this.finish();
    }
*/

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }
    @Override
    protected void onDestroy() {
        disposable.dispose();
        super.onDestroy();
    }

    public T getViewDataBinding() {
        return mViewDataBinding;
    }
    private void performDataBinding() {
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayout());
        this.mViewModel = mViewModel == null ? getViewModel() : mViewModel;
        mViewDataBinding.setVariable(getBindingVariable(), mViewModel);
        mViewDataBinding.executePendingBindings();
    }
}
