package g22.renzo.base;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.ViewModel;


import java.lang.ref.WeakReference;

import g22.renzo.store.repo.AppRepo;
import g22.renzo.utils.SessionManager;
import g22.renzo.utils.rx.SchedulerProvider;
import io.reactivex.disposables.CompositeDisposable;


public abstract class BaseViewModel<N> extends ViewModel {


    private final ObservableBoolean mIsLoading = new ObservableBoolean();

    private final SchedulerProvider mSchedulerProvider;

    private final AppRepo repo;

    private CompositeDisposable mCompositeDisposable;

    private WeakReference<N> mNavigator;

    public BaseViewModel(AppRepo repo, SchedulerProvider schedulerProvider) {
        this.mSchedulerProvider = schedulerProvider;
        this.repo=repo;
        this.mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        mCompositeDisposable.dispose();
        super.onCleared();
    }


    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }

    public ObservableBoolean getIsLoading() {
        return mIsLoading;
    }

    public void setIsLoading(boolean isLoading) {
        mIsLoading.set(isLoading);
    }

    public N getNavigator() {
        return mNavigator.get();
    }

    public void setNavigator(N navigator) {
        this.mNavigator = new WeakReference<>(navigator);
    }

    public SchedulerProvider getSchedulerProvider() {
        return mSchedulerProvider;
    }

    public AppRepo getRepo() {
        return repo;
    }
}
