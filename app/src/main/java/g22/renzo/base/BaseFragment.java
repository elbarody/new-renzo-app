package g22.renzo.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.marcinorlowski.fonty.Fonty;

import io.reactivex.disposables.CompositeDisposable;

public abstract class BaseFragment extends Fragment implements BaseView {

    public CompositeDisposable disposable;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(getLayout(), container, false);
        Fonty.setFonts((ViewGroup) view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        disposable = new CompositeDisposable();

        onCreateFragmentComponent();

    }

    @LayoutRes
    protected abstract int getLayout();


    protected abstract void onCreateFragmentComponent();


    @Override
    public void showErrorMessage(String msg) {

    }

    @Override
    public void showSuccessMessage(String msg) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void onDestroyView() {
        disposable.dispose();
        super.onDestroyView();
    }
}
