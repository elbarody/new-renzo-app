package g22.renzo.mydi.activity;

import android.content.Context;

import androidx.fragment.app.FragmentManager;


import dagger.Module;
import dagger.Provides;
import g22.renzo.mybase.BaseActivity;
import g22.renzo.mydi.qualifier.ForActivity;
import g22.renzo.mydi.scope.ActivityScope;
import g22.renzo.mystore.network.NetworkManager;
import g22.renzo.mystore.repo.HomeRepo;
import g22.renzo.mystore.repo.UserRepo;
import g22.renzo.utils.PreferencesUtil;

/**
 * This class is responsible for providing the requested objects to {@link ActivityScope} annotated classes
 */
@Module(includes = {ViewModelModule.class, NetworkModule.class})
public class
ActivityModule {

    private final BaseActivity activity;

    public ActivityModule(BaseActivity activity) {
        this.activity = activity;
    }

    @ActivityScope
    @Provides
    BaseActivity provideActivity() {
        return activity;
    }

    @ActivityScope
    @ForActivity
    @Provides
    FragmentManager provideFragmentManager() {
        return activity.getSupportFragmentManager();
    }

    @ActivityScope
    @Provides
    @ForActivity
    Context provideActivityContext() {
        return activity;
    }

    @ActivityScope
    @Provides
    UserRepo provideUserRepo(NetworkManager apiService, PreferencesUtil preferencesUtil) {
        return new UserRepo(apiService, preferencesUtil);
    }

    @ActivityScope
    @Provides
    HomeRepo provideUHomeRepo(NetworkManager apiService, PreferencesUtil preferencesUtil) {
        return new HomeRepo(apiService, preferencesUtil);
    }

}