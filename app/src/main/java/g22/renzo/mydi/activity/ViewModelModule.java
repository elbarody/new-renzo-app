package g22.renzo.mydi.activity;

import androidx.lifecycle.ViewModel;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Map;

import javax.inject.Provider;

import dagger.MapKey;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;
import g22.renzo.mybase.ViewModelFactory;
import g22.renzo.mystore.repo.HomeRepo;
import g22.renzo.mystore.repo.UserRepo;
import g22.renzo.ui.favorites.FavoritesActivityViewModel;
import g22.renzo.ui.forgetPassword.ForgetPasswordActivity;
import g22.renzo.ui.forgetPassword.ForgetPasswordViewModel;
import g22.renzo.ui.login.LoginActivity;
import g22.renzo.ui.login.LoginViewModel;
import g22.renzo.ui.mainActivity.MainActivityViewModel;
import g22.renzo.ui.more.MoreActivityViewModel;
import g22.renzo.ui.productDetails.CarsDetailsActivityViewModel;
import g22.renzo.ui.registration.RegistrationViewModel;
import g22.renzo.ui.splash.SplashActivity;
import g22.renzo.ui.splash.SplashViewModel;


@Module
public class ViewModelModule {

    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    @MapKey
    @interface ViewModelKey {
        Class<? extends ViewModel> value();
    }

    @Provides
    ViewModelFactory viewModelFactory(Map<Class<? extends ViewModel>, Provider<ViewModel>> providerMap) {
        return new ViewModelFactory(providerMap);
    }

    @Provides
    @IntoMap
    @ViewModelKey(SplashViewModel.class)
    ViewModel splashViewModel(UserRepo userRepo) {
        return new SplashViewModel(userRepo);
    }

    @Provides
    @IntoMap
    @ViewModelKey(LoginViewModel.class)
    ViewModel loginViewModel(UserRepo userRepo) {
        return new LoginViewModel(userRepo);
    }

    @Provides
    @IntoMap
    @ViewModelKey(ForgetPasswordViewModel.class)
    ViewModel forgetPasswordViewModel(UserRepo userRepo) {
        return new ForgetPasswordViewModel(userRepo);
    }

    @Provides
    @IntoMap
    @ViewModelKey(RegistrationViewModel.class)
    ViewModel registrationViewModel(UserRepo userRepo) {
        return new RegistrationViewModel(userRepo);
    }

    @Provides
    @IntoMap
    @ViewModelKey(MainActivityViewModel.class)
    ViewModel mainActivityViewModel(HomeRepo homeRepo) {
        return new MainActivityViewModel(homeRepo);
    }

    @Provides
    @IntoMap
    @ViewModelKey(MoreActivityViewModel.class)
    ViewModel moreActivityViewModel(HomeRepo homeRepo) {
        return new MoreActivityViewModel(homeRepo);
    }

    @Provides
    @IntoMap
    @ViewModelKey(CarsDetailsActivityViewModel.class)
    ViewModel carsDetailsActivityViewModel(HomeRepo homeRepo) {
        return new CarsDetailsActivityViewModel(homeRepo);
    }

    @Provides
    @IntoMap
    @ViewModelKey(FavoritesActivityViewModel.class)
    ViewModel favoritesActivityViewModel(HomeRepo homeRepo) {
        return new FavoritesActivityViewModel(homeRepo);
    }

}
