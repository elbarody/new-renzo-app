/*
 * *
 *  * Created by Mina George.
 *  * Email : minageorge888@gmail.com
 *  * Copyright (c) 2019. All rights reserved.
 *
 */

package g22.renzo.mydi.activity;


import dagger.Subcomponent;
import g22.renzo.mydi.scope.ActivityScope;
import g22.renzo.ui.favorites.FavoritesActivity;
import g22.renzo.ui.forgetPassword.ForgetPasswordActivity;
import g22.renzo.ui.login.LoginActivity;
import g22.renzo.ui.mainActivity.MainActivity;
import g22.renzo.ui.more.MoreActivity;
import g22.renzo.ui.productDetails.CarDetailsActivity;
import g22.renzo.ui.registration.RegistrationActivity;
import g22.renzo.ui.splash.SplashActivity;

@ActivityScope
@Subcomponent(modules = {ActivityModule.class})
public interface ActivityComponent {


    void inject(SplashActivity splashActivity);
    void inject(LoginActivity loginActivity);
    void inject(ForgetPasswordActivity forgetPasswordActivity);
    void inject(RegistrationActivity registrationActivity);
    void inject(MainActivity mainActivity);
    void inject(MoreActivity moreActivity);
    void inject(CarDetailsActivity carDetailsActivity);
    void inject(FavoritesActivity favoritesActivity);

}
