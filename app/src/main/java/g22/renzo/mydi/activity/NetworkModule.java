package g22.renzo.mydi.activity;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import g22.renzo.mydi.qualifier.ForActivity;
import g22.renzo.mydi.scope.ActivityScope;
import g22.renzo.mystore.network.APIService;
import g22.renzo.mystore.network.NetworkConstants;
import g22.renzo.mystore.network.NetworkManager;
import g22.renzo.utils.PreferencesUtil;
import io.reactivex.schedulers.Schedulers;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


@Module
public class NetworkModule {

    @ActivityScope
    @Provides
    OkHttpClient provideOkHttpClient(PreferencesUtil preferencesUtil) {
        return new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .callTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .addNetworkInterceptor(chain -> {
                    Request originalRequest = chain.request();
                    HttpUrl originalUrl = originalRequest.url();
                    HttpUrl url = originalUrl.newBuilder()
//                            .addQueryParameter("api_key", NetworkConstants.API_KEY)
//                            .addQueryParameter("region","PK")
//                            .addQueryParameter("language", preferencesUtil.getString(BaseActivity.LOCALE_KEY).equals("ar") ? "ar" : "en-US")
                            .build();
                    Request.Builder requestBuilder = originalRequest.newBuilder()
                            .url(url)
                            .addHeader("accept", "application/json")
                            .addHeader("Content-Type", "application/json");
                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }).build();
    }

    @ActivityScope
    @Provides
    @ForActivity
    APIService provideAPIService(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(NetworkConstants.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.newThread()))
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(APIService.class);
    }

    @ActivityScope
    @Provides
    @ForActivity
    NetworkManager provideNetworkManager(APIService apiService) {
        return new NetworkManager(apiService);
    }

}
