package g22.renzo.mydi.application;


import dagger.Component;
import g22.renzo.RenzoApplication;
import g22.renzo.mydi.activity.ActivityComponent;
import g22.renzo.mydi.activity.ActivityModule;
import g22.renzo.mydi.scope.ApplicationScope;

@ApplicationScope
@Component(modules = {AppModule.class})
public interface AppComponent {

    ActivityComponent plus(ActivityModule activityModule);

    void inject(RenzoApplication app);

}