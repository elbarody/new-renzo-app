package g22.renzo.mydi.application;

import android.app.Application;
import android.content.Context;

import dagger.Module;
import dagger.Provides;
import g22.renzo.RenzoApplication;
import g22.renzo.mydi.qualifier.ForApplication;
import g22.renzo.mydi.scope.ApplicationScope;
import g22.renzo.utils.PreferencesUtil;

/**
 * This class is responsible for providing the requested objects to {@link ApplicationScope} annotated classes
 */

@Module
public class AppModule {

    private final RenzoApplication application;

    public AppModule(RenzoApplication application) {
        this.application = application;
    }

    @ApplicationScope
    @Provides
    Application providesApplication() {
        return application;
    }

    @ApplicationScope
    @Provides
    @ForApplication
    Context providesApplicationContext() {
        return application;
    }

    @ApplicationScope
    @Provides
    PreferencesUtil providesPreferencesUtil() {
        return new PreferencesUtil(application);
    }

}