package g22.renzo.ui.splash;

import androidx.lifecycle.MutableLiveData;

import g22.renzo.mybase.BaseViewModel;
import g22.renzo.mystore.repo.UserRepo;
import g22.renzo.store.model.loginModels.UserData;

public class SplashViewModel extends BaseViewModel {

    private final UserRepo userRepo;

    MutableLiveData<UserData> userDataMutableLiveData = new MutableLiveData<>();

    public SplashViewModel(UserRepo userRepo) {
        this.userRepo = userRepo;
        this.userDataMutableLiveData.setValue(userRepo.getUserData());
    }


}
