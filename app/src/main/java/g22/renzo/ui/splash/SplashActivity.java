
package g22.renzo.ui.splash;

import android.content.Intent;
import android.os.Handler;

import g22.renzo.R;
import g22.renzo.RenzoApplication;
import g22.renzo.databinding.ActivitySplashBinding;
import g22.renzo.mybase.BaseActivity;
import g22.renzo.mydi.activity.ActivityModule;
import g22.renzo.ui.login.LoginActivity;
import g22.renzo.ui.mainActivity.MainActivity;


public class SplashActivity extends BaseActivity<SplashViewModel, ActivitySplashBinding> {


    @Override
    protected Class<SplashViewModel> getViewModelClass() {
        return SplashViewModel.class;
    }

    @Override
    protected void initActivityComponent() {
        component = RenzoApplication.getComponent(this)
                .plus(new ActivityModule(this));
        component.inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void initActivity() {

        new Handler().postDelayed(() -> {
            Intent intent;
            if (viewModel.userDataMutableLiveData.getValue() != null)
                intent= new Intent(this, MainActivity.class);
            else intent= new Intent(this, LoginActivity.class);

            startActivity(intent);
            finish();
        }, 1500);
    }
}
