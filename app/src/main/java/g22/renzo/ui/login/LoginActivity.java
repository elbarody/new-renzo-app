package g22.renzo.ui.login;

import android.content.Intent;
import android.widget.Toast;

import g22.renzo.R;
import g22.renzo.RenzoApplication;
import g22.renzo.databinding.ActivityLoginBinding;
import g22.renzo.mybase.BaseActivity;
import g22.renzo.mydi.activity.ActivityModule;
import g22.renzo.ui.forgetPassword.ForgetPasswordActivity;
import g22.renzo.ui.mainActivity.MainActivity;
import g22.renzo.ui.registration.RegistrationActivity;

public class LoginActivity extends BaseActivity<LoginViewModel, ActivityLoginBinding> {
    @Override
    protected Class<LoginViewModel> getViewModelClass() {
        return LoginViewModel.class;
    }

    @Override
    protected void initActivityComponent() {
        component = RenzoApplication.getComponent(this)
                .plus(new ActivityModule(this));
        component.inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initActivity() {

        dataBindingView.resisterTxt.setOnClickListener(v -> startActivity(new Intent(this, RegistrationActivity.class)));

        dataBindingView.forgetPasswordTv.setOnClickListener(v -> startActivity(new Intent(this, ForgetPasswordActivity.class)));

        dataBindingView.loginBtn.setOnClickListener(v -> {
            //TODO ADD VALIDATION
            if(!dataBindingView.inputEmail.getText().toString().isEmpty()&&!dataBindingView.inputPassword.getText().toString().isEmpty())
                viewModel.login(dataBindingView.inputEmail.getText().toString(),dataBindingView.inputPassword.getText().toString());
        });

    }

    @Override
    public void initObservers() {
        super.initObservers();
        viewModel.finishActivityLiveData.observe(this,state->{
            if(state){
                Toast.makeText(this, "Login success", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this, MainActivity.class));
                finish();
            }
        });
    }
}
