package g22.renzo.ui.login;

import g22.renzo.mybase.BaseViewModel;
import g22.renzo.mystore.repo.UserRepo;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class LoginViewModel extends BaseViewModel {

    private final UserRepo userRepo;

    public LoginViewModel(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public void login(String email, String pass) {

        addToDisposable(userRepo.login(email, pass)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> loadingLiveData.setValue(true))
                .doFinally(()->loadingLiveData.setValue(false))
                .subscribe(userResponse -> finishActivityLiveData.setValue(true), this::processError));
    }
}
