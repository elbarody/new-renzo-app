package g22.renzo.ui.favorites;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import g22.renzo.R;
import g22.renzo.RenzoApplication;
import g22.renzo.databinding.ActivityFavouritsBinding;
import g22.renzo.mybase.BaseActivity;
import g22.renzo.mybase.ViewModelFactory;
import g22.renzo.mydi.activity.ActivityModule;
import g22.renzo.ui.mainActivity.cars.TopCarAdapter;
import g22.renzo.ui.mainActivity.property.TopPropertyAdapter;

public class FavoritesActivity extends BaseActivity<FavoritesActivityViewModel, ActivityFavouritsBinding> {

    private TopCarAdapter topCarAdapter = new TopCarAdapter();
    private TopPropertyAdapter topPropertyAdapter = new TopPropertyAdapter();

    @Override
    protected Class<FavoritesActivityViewModel> getViewModelClass() {
        return FavoritesActivityViewModel.class;
    }

    @Override
    protected void initActivityComponent() {
        component = RenzoApplication.getComponent(this)
                .plus(new ActivityModule(this));
        component.inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_favourits;
    }

    @Override
    protected void initActivity() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(RecyclerView.VERTICAL);
        dataBindingView.favoritesRecyclerView.setLayoutManager(mLayoutManager);
        dataBindingView.favoritesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        dataBindingView.favoritesRecyclerView.setAdapter(topCarAdapter);

        dataBindingView.carTv.setOnClickListener(v -> {
            dataBindingView.favoritesRecyclerView.setAdapter(topCarAdapter);
            dataBindingView.carTv.setBackground(getDrawable(R.drawable.background_rounded_orang));
            dataBindingView.propertiesTv.setBackground(null);
        });
        dataBindingView.propertiesTv.setOnClickListener(v -> {
            dataBindingView.favoritesRecyclerView.setAdapter(topPropertyAdapter);
            dataBindingView.propertiesTv.setBackground(getDrawable(R.drawable.background_rounded_orang));
            dataBindingView.carTv.setBackground(null);

        });
    }

    @Override
    public void initObservers() {
        super.initObservers();
        viewModel.favoritesCarsListMutableLiveData.observe(this, carsModels -> {
            if (carsModels != null) {
                topCarAdapter.addItems(carsModels);
            }
        });

        viewModel.favoritesPropertiesListMutableLiveData.observe(this, propertiesModels -> {
            if (propertiesModels != null) {
                topPropertyAdapter.addItems(propertiesModels);
            }
        });
    }

    //    @Inject
//    LinearLayoutManager mLayoutManager;
//    @Inject
//    ViewModelFactory factory;
//    @Inject
//    TopPropertyAdapter topPropertyAdapter;
//
//    FavoritesActivityViewModel viewModel;
//    private ActivityFavouritsBinding activityMorePropertiesBinding;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        activityMorePropertiesBinding = getViewDataBinding();
//        viewModel.setNavigator(this);
//        setUp();
//    }
//
//    private void setUp() {
//        activityMorePropertiesBinding.setLifecycleOwner(this);
//
//        mLayoutManager.setOrientation(RecyclerView.VERTICAL);
//        activityMorePropertiesBinding.favoritesRecyclerView.setLayoutManager(mLayoutManager);
//        activityMorePropertiesBinding.favoritesRecyclerView.setItemAnimator(new DefaultItemAnimator());
//        activityMorePropertiesBinding.favoritesRecyclerView.setAdapter(topPropertyAdapter);
//    }
//
//    @Override
//    public int getBindingVariable() {
//        return BR.viewModel;
//    }
//
//    @Override
//    public FavoritesActivityViewModel getViewModel() {
//        viewModel = ViewModelProviders.of(this, factory).get(FavoritesActivityViewModel.class);
//        return viewModel;
//    }
//
//    @Override
//    protected int getLayout() {
//        return R.layout.activity_favourits;
//    }
//
//    @Override
//    protected void onCreateActivityComponent() {
//
//    }
}
