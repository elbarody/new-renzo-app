package g22.renzo.ui.favorites;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import g22.renzo.mybase.BaseViewModel;
import g22.renzo.mystore.models.home.HomeResponse;
import g22.renzo.mystore.repo.HomeRepo;
import g22.renzo.store.model.carModels.CarsModel;
import g22.renzo.store.model.property.PropertiesModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class FavoritesActivityViewModel extends BaseViewModel {
    private final HomeRepo homeRepo;
    final MutableLiveData<List<CarsModel>> favoritesCarsListMutableLiveData = new MutableLiveData<>();
    final MutableLiveData<List<PropertiesModel>> favoritesPropertiesListMutableLiveData = new MutableLiveData<>();

    public FavoritesActivityViewModel(HomeRepo homeRepo) {
        this.homeRepo = homeRepo;
        getFavoritesCars();
        getFavoritesPrperties();
    }

    private void getFavoritesCars() {
        addToDisposable(homeRepo.getFavoriteCars()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> loadingLiveData.setValue(true))
                .doFinally(()->loadingLiveData.setValue(false))
                .subscribe(response -> {
                    favoritesCarsListMutableLiveData.setValue(response.getFavs().getData());
                }, this::processError));
    }

    private void getFavoritesPrperties() {
        addToDisposable(homeRepo.getFavoritePrperties()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> loadingLiveData.setValue(true))
                .doFinally(()->loadingLiveData.setValue(false))
                .subscribe(response -> {
                    favoritesPropertiesListMutableLiveData.setValue(response.getFavs().getPropertiesData());
                }, this::processError));
    }
//    private final MutableLiveData<List<CarsModel>> carResponseMutableLiveData;
//    private String token;
//    public FavoritesActivityViewModel(AppRepo repo, SchedulerProvider schedulerProvider) {
//        super(repo, schedulerProvider);
//        carResponseMutableLiveData = new MutableLiveData<>();
//        getPropertyData();
//    }
//
//    public void getPropertyData() {
//        getRepo().getFavorites("","car");
//
//        //getNavigator().getLogin();
//    }
//
//    public LiveData<List<PropertiesModel>> getPropertyListLiveData() {
//        //TODO return response
//        return null;
//        //return getRepo().topPropertyMutableLiveData;
//    }

}
