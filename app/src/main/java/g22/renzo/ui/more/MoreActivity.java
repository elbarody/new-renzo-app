package g22.renzo.ui.more;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import g22.renzo.R;
import g22.renzo.RenzoApplication;
import g22.renzo.databinding.ActivityMoreCarsBinding;
import g22.renzo.mybase.BaseActivity;
import g22.renzo.mydi.activity.ActivityModule;
import g22.renzo.ui.mainActivity.cars.TopCarAdapter;
import g22.renzo.ui.mainActivity.property.TopPropertyAdapter;

public class MoreActivity extends BaseActivity<MoreActivityViewModel, ActivityMoreCarsBinding> {


    TopCarAdapter topCarAdapter = new TopCarAdapter();
    TopPropertyAdapter topPropertyAdapter = new TopPropertyAdapter();

    @Override
    protected Class<MoreActivityViewModel> getViewModelClass() {
        return MoreActivityViewModel.class;
    }

    @Override
    protected void initActivityComponent() {
        component = RenzoApplication.getComponent(this)
                .plus(new ActivityModule(this));
        component.inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_more_cars;
    }

    @Override
    protected void initActivity() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dataBindingView.moreCarRecyclerView.setLayoutManager(linearLayoutManager);
        dataBindingView.moreCarRecyclerView.setItemAnimator(new DefaultItemAnimator());
        switch (getIntent().getIntExtra("type", 0)) {

            case 1:
                dataBindingView.titleTextView.setText(getString(R.string.cars));
                dataBindingView.moreCarRecyclerView.setAdapter(topCarAdapter);
                viewModel.getCars();
                break;

            case 2:
                dataBindingView.titleTextView.setText(getString(R.string.properties));
                dataBindingView.moreCarRecyclerView.setAdapter(topPropertyAdapter);
                viewModel.getProperties();
        }
    }

    @Override
    public void initObservers() {
        super.initObservers();
        viewModel.carsListMutableLiveData.observe(this, carsModels -> {
            if (carsModels != null) {
                topCarAdapter.addItems(carsModels);
            }
        });
        viewModel.propertiesListMutableLiveData.observe(this, properties -> {
            if (properties != null) {
                topPropertyAdapter.addItems(properties);
            }
        });
    }

    //
//    @Inject
//    LinearLayoutManager layoutManager;
//    @Inject
//    ViewModelFactory factory;
//    @Inject
//    TopCarAdapter topCarAdapter;
//
//
//    MoreActivityViewModel viewModel;
//    private ActivityMoreCarsBinding activityMoreCarsBinding;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        activityMoreCarsBinding = getViewDataBinding();
//        viewModel.setNavigator(this);
//        setUp();
//    }
//
//    @Override
//    public int getBindingVariable() {
//        return BR.viewModel;
//    }
//
//    @Override
//    public MoreActivityViewModel getViewModel() {
//        viewModel = ViewModelProviders.of(this, factory).get(MoreActivityViewModel.class);
//        return viewModel;
//    }
//
//    @Override
//    protected int getLayout() {
//        return R.layout.activity_more_cars;
//    }
//
//    @Override
//    protected void onCreateActivityComponent() {
//
//    }
//
//    @Override
//    public void openLoginActivity() {
//
//    }
//
//
//    private void setUp() {
//        activityMoreCarsBinding.setLifecycleOwner(this);
//
//
//    }
}
