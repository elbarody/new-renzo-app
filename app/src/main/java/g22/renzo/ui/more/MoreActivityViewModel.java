package g22.renzo.ui.more;


import androidx.lifecycle.MutableLiveData;

import java.util.List;

import g22.renzo.mybase.BaseViewModel;
import g22.renzo.mystore.models.home.HomeResponse;
import g22.renzo.mystore.repo.HomeRepo;
import g22.renzo.store.model.carModels.CarsModel;
import g22.renzo.store.model.property.PropertiesModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MoreActivityViewModel extends BaseViewModel {

    private final HomeRepo homeRepo;
    final MutableLiveData<List<CarsModel>> carsListMutableLiveData = new MutableLiveData<>();
    final MutableLiveData<List<PropertiesModel>> propertiesListMutableLiveData = new MutableLiveData<>();

    public MoreActivityViewModel(HomeRepo homeRepo) {
        this.homeRepo = homeRepo;
    }

    public void getProperties() {
        addToDisposable(homeRepo.getAllPrperties()
                .map(getAllPropertiesResponse -> getAllPropertiesResponse.getProperties().getData())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> loadingLiveData.setValue(true))
                .doFinally(() -> loadingLiveData.setValue(false))
                .subscribe(propertiesListMutableLiveData::setValue, this::processError));
    }

    public void getCars() {
        addToDisposable(homeRepo.getAllCars()
                .map(getAllPropertiesResponse -> getAllPropertiesResponse.getCars().getData())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> loadingLiveData.setValue(true))
                .doFinally(() -> loadingLiveData.setValue(false))
                .subscribe(carsListMutableLiveData::setValue, this::processError));
    }

}
