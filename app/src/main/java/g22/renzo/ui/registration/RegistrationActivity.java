package g22.renzo.ui.registration;


import g22.renzo.R;
import g22.renzo.RenzoApplication;
import g22.renzo.databinding.ActivityRegistrationBinding;
import g22.renzo.mybase.BaseActivity;
import g22.renzo.mydi.activity.ActivityModule;

public class RegistrationActivity extends BaseActivity<RegistrationViewModel, ActivityRegistrationBinding> {

    @Override
    protected Class<RegistrationViewModel> getViewModelClass() {
        return RegistrationViewModel.class;
    }

    @Override
    protected void initActivityComponent() {
        component = RenzoApplication.getComponent(this)
                .plus(new ActivityModule(this));
        component.inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_registration;
    }

    @Override
    protected void initActivity() {
        // TODO Handle validation.
        dataBindingView.signUpBtn.setOnClickListener(v -> {
            if (!dataBindingView.inputName.getText().toString().isEmpty() && !dataBindingView.inputLastName.getText().toString().isEmpty()
                    && !dataBindingView.inputEmail.getText().toString().isEmpty() && !dataBindingView.inputPassword.getText().toString().isEmpty()) {
                viewModel.register(dataBindingView.inputName.getText().toString(), dataBindingView.inputLastName.getText().toString(), dataBindingView.inputEmail.getText().toString(), dataBindingView.inputPassword.getText().toString());
            }
        });

    }

    @Override
    public void initObservers() {
        super.initObservers();
        viewModel.finishActivityLiveData.observe(this, state -> {
            if (state) {
                finish();
            }
        });
    }
}
