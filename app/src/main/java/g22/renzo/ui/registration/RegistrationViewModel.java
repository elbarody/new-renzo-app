package g22.renzo.ui.registration;


import g22.renzo.mybase.BaseViewModel;
import g22.renzo.mystore.repo.UserRepo;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class RegistrationViewModel extends BaseViewModel {

    private final UserRepo userRepo;

    public RegistrationViewModel(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public void register(String f_name, String l_name, String email, String password) {
        addToDisposable(userRepo.register(f_name, l_name, email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> loadingLiveData.setValue(true))
                .doFinally(() -> loadingLiveData.setValue(false))
                .subscribe(response -> {
                    successMsgLiveData.setValue(response.getSuccess());
                    finishActivityLiveData.setValue(true);
                }, this::processError));
    }

}