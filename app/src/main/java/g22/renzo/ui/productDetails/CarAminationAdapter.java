package g22.renzo.ui.productDetails;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import g22.renzo.base.BaseViewHolder;
import g22.renzo.databinding.AminiteCarItemBinding;
import g22.renzo.databinding.ItemEmptyBinding;
import g22.renzo.store.model.carModels.AminitiesModel;
import g22.renzo.ui.mainActivity.EmptyItemViewModel;
import g22.renzo.ui.mainActivity.cars.TopCarAdapter;

public class CarAminationAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private List<AminitiesModel> items = new ArrayList<>();
    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;


    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                AminiteCarItemBinding binding = AminiteCarItemBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new AminitiesViewHolder(binding);
            case VIEW_TYPE_EMPTY:
            default:
                ItemEmptyBinding emptyViewBinding = ItemEmptyBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new EmptyViewHolder(emptyViewBinding);
        }
    }

    public void addItems(List<AminitiesModel> carsModels) {
        if (carsModels != null) {
            items.addAll(carsModels);
            notifyDataSetChanged();
        }
    }

    public void clearItems() {
        items.clear();
    }


    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);

    }

    @Override
    public int getItemCount() {
        if (items != null && items.size() > 0) {
            return items.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (items != null && !items.isEmpty()) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    public class AminitiesViewHolder extends BaseViewHolder {

        private AminiteCarItemBinding mBinding;

        private AminitiesItemViewModel viewModel;


        public AminitiesViewHolder(AminiteCarItemBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            final AminitiesModel blog = items.get(position);
            viewModel = new AminitiesItemViewModel(blog);
            mBinding.setViewModel(viewModel);

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();
        }

    }

    public class EmptyViewHolder extends BaseViewHolder {

        private ItemEmptyBinding mBinding;

        public EmptyViewHolder(ItemEmptyBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            EmptyItemViewModel emptyItemViewModel = new EmptyItemViewModel();
            mBinding.setViewModel(emptyItemViewModel);
        }

    }


}
