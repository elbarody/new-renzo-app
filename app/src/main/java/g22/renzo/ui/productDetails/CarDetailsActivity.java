package g22.renzo.ui.productDetails;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;


import javax.inject.Inject;

import g22.renzo.BR;
import g22.renzo.R;
import g22.renzo.RenzoApplication;
import g22.renzo.databinding.ActivityDetailsBinding;
import g22.renzo.mybase.BaseActivity;
import g22.renzo.mybase.ViewModelFactory;
import g22.renzo.mydi.activity.ActivityModule;

public class CarDetailsActivity extends BaseActivity<CarsDetailsActivityViewModel, ActivityDetailsBinding> {

    CarAminationAdapter adapter = new CarAminationAdapter();

    @Override
    protected Class<CarsDetailsActivityViewModel> getViewModelClass() {
        return CarsDetailsActivityViewModel.class;
    }

    @Override
    protected void initActivityComponent() {
        component = RenzoApplication.getComponent(this)
                .plus(new ActivityModule(this));
        component.inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_details;
    }

    @Override
    protected void initActivity() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        dataBindingView.optionRv.setLayoutManager(layoutManager);
        dataBindingView.optionRv.setItemAnimator(new DefaultItemAnimator());
        dataBindingView.optionRv.setAdapter(adapter);

        viewModel.getCarDetailsData(getIntent().getIntExtra("car_id", 0));
    }

    @Override
    public void initObservers() {
        super.initObservers();
        viewModel.carsModelMutableLiveData.observe(this, carsModel -> {
            if (carsModel != null) {
                dataBindingView.setCarModel(carsModel);
            }
        });
    }


    //    @Inject
//    LinearLayoutManager layoutManager;
//    @Inject
//    ViewModelFactory factory;
//    @Inject
//    CarAminationAdapter adapter;
//
//
//    CarsDetailsActivityViewModel viewModel;
//    private ActivityDetailsBinding binding;
//    private int id;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        binding = getViewDataBinding();
//        viewModel.setNavigator(this);
//        getViewModel().getCarDetailsData(id);
//        setUp();
//    }
//
//    @Override
//    public int getBindingVariable() {
//        return BR.viewModel;
//    }
//
//    @Override
//    public CarsDetailsActivityViewModel getViewModel() {
//        viewModel = ViewModelProviders.of(this, factory).get(CarsDetailsActivityViewModel.class);
//        return viewModel;
//    }
//
//    @Override
//    protected int getLayout() {
//        return R.layout.activity_details;
//    }
//
//
//    @Override
//    protected void onCreateActivityComponent() {
//        id=getIntent().getIntExtra("car_id", 0);
//    }
//
//    @Override
//    public void openLoginActivity() {
//
//        getViewModel().getCarDetailsResponseMutableLiveData.observe(this,getCarDetailsResponse -> {
//            if (getCarDetailsResponse.getStatus()==200){
//                getViewModel().addData(getCarDetailsResponse);
//            }
//        });
//    }
//
//    @Override
//    public int getItemId() {
//        return 0;
//    }
//
//    private void setUp() {
//        binding.setLifecycleOwner(this);
//        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
//        binding.optionRv.setLayoutManager(layoutManager);
//        binding.optionRv.setItemAnimator(new DefaultItemAnimator());
//        binding.optionRv.setAdapter(adapter);
//
//    }
}
