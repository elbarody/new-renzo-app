package g22.renzo.ui.productDetails;

import java.util.List;

import g22.renzo.store.model.carModels.CarsModel;


public interface CarsDetailsNavigator {
    void openLoginActivity();

    int getItemId();

}
