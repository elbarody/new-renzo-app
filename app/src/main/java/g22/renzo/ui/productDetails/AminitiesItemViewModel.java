package g22.renzo.ui.productDetails;

import androidx.databinding.ObservableField;

import g22.renzo.store.model.carModels.AminitiesModel;

public class AminitiesItemViewModel {
    public final ObservableField<String> name;

    public AminitiesItemViewModel(AminitiesModel model) {
        name = new ObservableField<>(model.getName());
    }
}
