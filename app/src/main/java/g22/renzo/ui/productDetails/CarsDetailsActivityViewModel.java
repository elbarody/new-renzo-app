/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package g22.renzo.ui.productDetails;


import androidx.lifecycle.MutableLiveData;

import java.util.List;

import g22.renzo.mybase.BaseViewModel;
import g22.renzo.mystore.repo.HomeRepo;
import g22.renzo.store.model.carModels.CarsModel;
import g22.renzo.store.model.property.PropertiesModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CarsDetailsActivityViewModel extends BaseViewModel {

    private final HomeRepo homeRepo;
    MutableLiveData<CarsModel> carsModelMutableLiveData = new MutableLiveData<>();

    public CarsDetailsActivityViewModel(HomeRepo homeRepo) {
        this.homeRepo = homeRepo;
    }


    public void getCarDetailsData(int id) {
        addToDisposable(homeRepo.getCarById(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> loadingLiveData.setValue(true))
                .doFinally(() -> loadingLiveData.setValue(false))
                .subscribe(carsModelMutableLiveData::setValue, this::processError));
    }


//    public final MutableLiveData<GetCarDetailsResponse> getCarDetailsResponseMutableLiveData;
//    public int id;
//    public LiveData<String> carBrand;
//    public LiveData<String> model;
//    public LiveData<String> price;
//    public LiveData<String> rate;
//    public LiveData<String> ratesNumber;
//    public LiveData<String> city;
//    public LiveData<String> area;
//    public LiveData<String> trips;
//    public LiveData<String> imageUrl;
//    public LiveData<List<AminitiesModel>> aminitiesLiveData;
//    MediatorLiveData<GetCarDetailsResponse> data;
//    private String token;
//
//    public CarsDetailsActivityViewModel(AppRepo repo, SchedulerProvider schedulerProvider) {
//        super(repo, schedulerProvider);
//        getCarDetailsResponseMutableLiveData = getRepo().carDetailsMutableLiveData;
//    }
//
//
//    public void getCarDetailsData(int id) {
//        getRepo().getCarDetails(id, "");
//
//        getNavigator().openLoginActivity();
//        data=new MediatorLiveData<>();
//        data.addSource(getRepo().carDetailsMutableLiveData, getCarDetailsResponse -> {
//            data.setValue(getCarDetailsResponse);
//            if (getCarDetailsResponse.getStatus() == 200) {
//                carBrand = new MutableLiveData<>(data.getValue().getCar().getMake().getName());
//                model = new MutableLiveData<>(data.getValue().getCar().getModel().getName());
//                price = new MutableLiveData<>(String.valueOf(data.getValue().getCar().getPrice()));
//                rate = new MutableLiveData<>(String.valueOf(data.getValue().getCar().getRate()));
//                ratesNumber = new MutableLiveData<>(String.valueOf(data.getValue().getCar().getRates_number()));
//                city = new MutableLiveData<>(data.getValue().getCar().getCity().getName());
//                area = new MutableLiveData<>(data.getValue().getCar().getArea().getName());
//                trips = new MutableLiveData<>(String.valueOf(data.getValue().getCar().getTrips()));
//                imageUrl = new MutableLiveData<>(data.getValue().getCar().getImages().get(0).getImage());
//                aminitiesLiveData = new MutableLiveData<>(data.getValue().getCar().getAminities());
//            }
//        });
//}
//
//
//    public LiveData<List<CarsModel>> getCarsListLiveData() {
//        return getRepo().topCarLiveDataMutableLiveData;
//    }
//
//    public void addData(GetCarDetailsResponse getCarDetailsResponse) {
//        data.setValue(getCarDetailsResponse);
//        carBrand = new MutableLiveData<>(data.getValue().getCar().getMake().getName());
//        model = new MutableLiveData<>(data.getValue().getCar().getModel().getName());
//        price = new MutableLiveData<>(String.valueOf(data.getValue().getCar().getPrice()));
//        rate = new MutableLiveData<>(String.valueOf(data.getValue().getCar().getRate()));
//        ratesNumber = new MutableLiveData<>(String.valueOf(data.getValue().getCar().getRates_number()));
//        city = new MutableLiveData<>(data.getValue().getCar().getCity().getName());
//        area = new MutableLiveData<>(data.getValue().getCar().getArea().getName());
//        trips = new MutableLiveData<>(String.valueOf(data.getValue().getCar().getTrips()));
//        imageUrl = new MutableLiveData<>(data.getValue().getCar().getImages().get(0).getImage());
//        aminitiesLiveData = new MutableLiveData<>(data.getValue().getCar().getAminities());
//    }
//
//    public LiveData<String> getCarBrand() {
//        return carBrand;
//    }
//
//    public LiveData<String> getModel() {
//        return model;
//    }
//
//    public LiveData<String> getPrice() {
//        return price;
//    }
//
//    public LiveData<String> getRate() {
//        return rate;
//    }
//
//    public LiveData<String> getRatesNumber() {
//        return ratesNumber;
//    }
//
//    public LiveData<String> getCity() {
//        return city;
//    }
//
//    public LiveData<String> getArea() {
//        return area;
//    }
//
//    public LiveData<String> getTrips() {
//        return trips;
//    }
//
//    public LiveData<String> getImageUrl() {
//        return imageUrl;
//    }
//
//    public LiveData<List<AminitiesModel>> getAminitiesLiveData() {
//        return aminitiesLiveData;
//    }
}
