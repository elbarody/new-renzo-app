package g22.renzo.ui.forgetPassword;

import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import javax.inject.Inject;

import g22.renzo.BR;
import g22.renzo.R;
import g22.renzo.RenzoApplication;
import g22.renzo.databinding.ActivityForgetPasswordBinding;
import g22.renzo.mybase.BaseActivity;
import g22.renzo.mydi.activity.ActivityModule;
import g22.renzo.ui.login.LoginActivity;
import g22.renzo.ui.mainActivity.MainActivity;

public class ForgetPasswordActivity extends BaseActivity<ForgetPasswordViewModel, ActivityForgetPasswordBinding> {

    @Override
    protected Class<ForgetPasswordViewModel> getViewModelClass() {
        return ForgetPasswordViewModel.class;
    }

    @Override
    protected void initActivityComponent() {
        component = RenzoApplication.getComponent(this)
                .plus(new ActivityModule(this));
        component.inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_forget_password;
    }

    @Override
    protected void initActivity() {

        dataBindingView.btnChange.setOnClickListener(v -> {
            // TODO Handle validation.
            if(!dataBindingView.inputEmail.getText().toString().isEmpty())
            viewModel.forgotPassword(dataBindingView.inputEmail.getText().toString());
        });
    }

    @Override
    public void initObservers() {
        super.initObservers();
        viewModel.finishActivityLiveData.observe(this,state->{
            if(state){
                finish();
            }
        });
    }
}
