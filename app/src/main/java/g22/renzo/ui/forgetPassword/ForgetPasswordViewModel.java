package g22.renzo.ui.forgetPassword;


import g22.renzo.mybase.BaseViewModel;
import g22.renzo.mystore.repo.UserRepo;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ForgetPasswordViewModel extends BaseViewModel {

    private final UserRepo userRepo;

    public ForgetPasswordViewModel(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public void forgotPassword(String email) {
        addToDisposable(userRepo.forgetPassword(email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> loadingLiveData.setValue(true))
                .doFinally(()->loadingLiveData.setValue(false))
                .subscribe(response -> {
                    successMsgLiveData.setValue(response.getSuccess());
                    finishActivityLiveData.setValue(true);
                }, this::processError));
    }
}
