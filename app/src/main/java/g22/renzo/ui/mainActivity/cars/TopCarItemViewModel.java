package g22.renzo.ui.mainActivity.cars;

import androidx.databinding.ObservableField;

import g22.renzo.store.model.carModels.CarsModel;

public class TopCarItemViewModel {


    public final ObservableField<String> imageUrl;

    public final TopCarItemViewModelListener mListener;

    public final ObservableField<String> carBrand;
    public final ObservableField<String> model;
    public final ObservableField<String> price;
    public final ObservableField<String> rate;
    public final ObservableField<String> ratesNumber;
    public final ObservableField<String> city;
    public final ObservableField<String> area;
    public final ObservableField<String> trips;

    private final CarsModel carsModel;
    private final ObservableField<Integer> id;

    public TopCarItemViewModel(CarsModel carsModel, TopCarItemViewModelListener listener) {
        this.carsModel = carsModel;
        this.mListener = listener;
        id=new ObservableField<>(this.carsModel.getId());
        imageUrl = new ObservableField<>(this.carsModel.getImages().get(0).getImage());
        carBrand = new ObservableField<>(this.carsModel.getMake().getName());
        model = new ObservableField<>(this.carsModel.getModel().getName());
        price = new ObservableField<>(Integer.toString(this.carsModel.getPrice()));
        rate = new ObservableField<>(Integer.toString(this.carsModel.getRate()));
        ratesNumber = new ObservableField<>("(" + this.carsModel.getRates_number() + ")");
        city = new ObservableField<>(this.carsModel.getCity().getName());
        area = new ObservableField<>(this.carsModel.getArea().getName());
        trips = new ObservableField<>("(" + this.carsModel.getTrips() + ")");
    }

    public void onItemClick() {
        mListener.onItemClick(id);
    }

    public interface TopCarItemViewModelListener {
        void onItemClick(ObservableField<Integer> id);
    }
}
