package g22.renzo.ui.mainActivity.cars;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.ObservableField;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.List;

import g22.renzo.base.BaseViewHolder;
import g22.renzo.databinding.ItemEmptyBinding;
import g22.renzo.databinding.ItemTopListOfCarBinding;
import g22.renzo.store.model.carModels.CarsModel;
import g22.renzo.ui.mainActivity.EmptyItemViewModel;
import g22.renzo.ui.productDetails.CarDetailsActivity;

public class TopCarAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;

    private List<CarsModel> topCarsItems = new ArrayList<>();

    private BlogAdapterListener mListener;


    @Override
    public int getItemCount() {
        if (topCarsItems != null && topCarsItems.size() > 0) {
            return topCarsItems.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (topCarsItems != null && !topCarsItems.isEmpty()) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                ItemTopListOfCarBinding carBinding = ItemTopListOfCarBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new CarsViewHolder(carBinding);
            case VIEW_TYPE_EMPTY:
            default:
                ItemEmptyBinding emptyViewBinding = ItemEmptyBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new EmptyViewHolder(emptyViewBinding);
        }
    }

    public void addItems(List<CarsModel> carsModels) {
        topCarsItems.addAll(carsModels);
        notifyDataSetChanged();
    }

    public void clearItems() {
        topCarsItems.clear();
    }

    public void setListener(BlogAdapterListener listener) {
        this.mListener = listener;
    }

    public interface BlogAdapterListener {

        void onRetryClick();
    }

    public class CarsViewHolder extends BaseViewHolder implements TopCarItemViewModel.TopCarItemViewModelListener {

        private ItemTopListOfCarBinding mBinding;

        private TopCarItemViewModel carItemViewModel;

        public CarsViewHolder(ItemTopListOfCarBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            final CarsModel blog = topCarsItems.get(position);
            carItemViewModel = new TopCarItemViewModel(blog, this);
            mBinding.setViewModel(carItemViewModel);

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();
        }

        @Override
        public void onItemClick(ObservableField<Integer> id) {
            itemView.getContext().startActivity(new Intent(itemView.getContext(), CarDetailsActivity.class).putExtra("car_id", id.get().intValue()));
        }
    }

    public class EmptyViewHolder extends BaseViewHolder implements EmptyItemViewModel.BlogEmptyItemViewModelListener {

        private ItemEmptyBinding mBinding;

        public EmptyViewHolder(ItemEmptyBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            EmptyItemViewModel emptyItemViewModel = new EmptyItemViewModel(this);
            mBinding.setViewModel(emptyItemViewModel);
        }

        @Override
        public void onRetryClick() {
            mListener.onRetryClick();
        }
    }
}