package g22.renzo.ui.mainActivity;


import androidx.lifecycle.MutableLiveData;

import g22.renzo.mybase.BaseViewModel;
import g22.renzo.mystore.models.home.HomeResponse;
import g22.renzo.mystore.repo.HomeRepo;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainActivityViewModel extends BaseViewModel {

    private final HomeRepo homeRepo;
    final MutableLiveData<HomeResponse> homeResponseMutableLiveData = new MutableLiveData<>();

    public MainActivityViewModel(HomeRepo homeRepo) {
        this.homeRepo = homeRepo;
        getHomeData();
    }

    private void getHomeData() {
        addToDisposable(homeRepo.getHomeData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> loadingLiveData.setValue(true))
                .doFinally(() -> loadingLiveData.setValue(false))
                .subscribe(homeResponseMutableLiveData::setValue, this::processError));

//
//    private final MutableLiveData<List<PropertiesModel>> propertyListMutableLiveData;
//    private String token;
//
//    public MainActivityViewModel(AppRepo repo, SchedulerProvider schedulerProvider) {
//        super(repo, schedulerProvider);
//        carResponseMutableLiveData = new MutableLiveData<>();
//        propertyListMutableLiveData = new MutableLiveData<>();
//        getCarData();
//        getPropertyData();
//    }
//
//
//    public void getCarData() {
//        getRepo().getTopCar("");
//        //this.carResponseMutableLiveData.setValue(.getValue());
//
//        //getNavigator().getLogin();
//    }
//
//    public void getPropertyData() {
//        getRepo().getTopProperty("");
//
//        //getNavigator().getLogin();
//    }
//
//    public LiveData<List<CarsModel>> getCarsListLiveData() {
//        return getRepo().topCarLiveDataMutableLiveData;
//    }
//
//    public LiveData<List<PropertiesModel>> getPropertyListLiveData() {
//        return getRepo().topPropertyMutableLiveData;
    }
}
