package g22.renzo.ui.mainActivity;

import android.content.Intent;

import android.view.View;

import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import android.view.Menu;

import g22.renzo.R;
import g22.renzo.RenzoApplication;
import g22.renzo.databinding.ActivityMainBinding;
import g22.renzo.mybase.BaseActivity;
import g22.renzo.mydi.activity.ActivityModule;
import g22.renzo.ui.favorites.FavoritesActivity;
import g22.renzo.ui.mainActivity.cars.TopCarAdapter;
import g22.renzo.ui.mainActivity.property.TopPropertyAdapter;
import g22.renzo.ui.more.MoreActivity;

public class MainActivity extends BaseActivity<MainActivityViewModel, ActivityMainBinding> {

    private TopCarAdapter topCarAdapter;
    private TopPropertyAdapter topPropertyAdapter;

    @Override
    protected Class<MainActivityViewModel> getViewModelClass() {
        return MainActivityViewModel.class;
    }

    @Override
    protected void initActivityComponent() {
        component = RenzoApplication.getComponent(this)
                .plus(new ActivityModule(this));
        component.inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initActivity() {

        setupDrawer();

    }

    @Override
    public void initObservers() {
        super.initObservers();
        viewModel.homeResponseMutableLiveData.observe(this, homeResponse -> {
            if (homeResponse != null) {
                if (homeResponse.getTopCarResponse() != null) {
                    topCarAdapter.addItems(homeResponse.getTopCarResponse().getCars());
                }
                if (homeResponse.getTopPropertyResponse() != null) {
                    topPropertyAdapter.addItems(homeResponse.getTopPropertyResponse().getProperties());
                }
            }

        });
    }

    @Override
    public void onBackPressed() {
        if (dataBindingView.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            dataBindingView.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (dataBindingView.drawerLayout != null) {
            dataBindingView.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }

    private void lockDrawer() {
        if (dataBindingView.drawerLayout != null) {
            dataBindingView.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }


    private void setupDrawer() {

        dataBindingView.menuImage.setOnClickListener(v -> {

            if (dataBindingView.drawerLayout.isDrawerOpen(dataBindingView.navView)) {
                dataBindingView.drawerLayout.closeDrawer(dataBindingView.navView);
            } else {
                dataBindingView.drawerLayout.openDrawer(dataBindingView.navView);
            }
        });

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this,
                dataBindingView.drawerLayout,
                R.string.drawer_open,
                R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                hideKeyboard();
            }
        };

        dataBindingView.drawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        dataBindingView.navView.findViewById(R.id.fav_tv).setOnClickListener(v -> startActivity(new Intent(this, FavoritesActivity.class)));

        dataBindingView.allCarBtn.setOnClickListener(view -> {
            Intent intent = new Intent(this, MoreActivity.class);
            intent.putExtra("type", 1);
            startActivity(intent);
        });

        dataBindingView.allPropertiesBtn.setOnClickListener(view -> {
            Intent intent = new Intent(this, MoreActivity.class);
            intent.putExtra("type", 2);
            startActivity(intent);
        });

        GridLayoutManager mLayoutManagerCar = new GridLayoutManager(this, 2);
        GridLayoutManager mLayoutManagerProperty = new GridLayoutManager(this, 2);

        topCarAdapter = new TopCarAdapter();
        topPropertyAdapter = new TopPropertyAdapter();
        dataBindingView.topCarList.setLayoutManager(mLayoutManagerCar);
        dataBindingView.topCarList.setItemAnimator(new DefaultItemAnimator());
        dataBindingView.topCarList.setAdapter(topCarAdapter);

        dataBindingView.topPropertyList.setLayoutManager(mLayoutManagerProperty);
        dataBindingView.topPropertyList.setItemAnimator(new DefaultItemAnimator());
        dataBindingView.topPropertyList.setAdapter(topPropertyAdapter);
    }
}
