package g22.renzo.ui.mainActivity;

public class EmptyItemViewModel {

    private BlogEmptyItemViewModelListener mListener;

    public EmptyItemViewModel(BlogEmptyItemViewModelListener listener) {
        this.mListener = listener;
    }

    public EmptyItemViewModel() {
    }
    public void onRetryClick() {
        mListener.onRetryClick();
    }

    public interface BlogEmptyItemViewModelListener {

        void onRetryClick();
    }
}
