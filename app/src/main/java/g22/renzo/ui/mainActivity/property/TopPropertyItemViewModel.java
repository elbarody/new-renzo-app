package g22.renzo.ui.mainActivity.property;

import androidx.databinding.ObservableField;

import g22.renzo.store.model.property.PropertiesModel;

public class TopPropertyItemViewModel {


    public final ObservableField<String> imageUrl;

    public final TopCarItemViewModelListener mListener;

    public final ObservableField<String> propertyType;
    public final ObservableField<String> homeType;
    public final ObservableField<String> price;
    public final ObservableField<String> rate;
    public final ObservableField<String> ratesNumber;
    public final ObservableField<String> city;
    public final ObservableField<String> area;
    public final ObservableField<String> trips;

    private final PropertiesModel propertyModel;

    public TopPropertyItemViewModel(PropertiesModel propertyModel, TopCarItemViewModelListener listener) {
        this.propertyModel = propertyModel;
        this.mListener = listener;
        imageUrl = new ObservableField<>(this.propertyModel.getImages().get(0).getImage());
        propertyType = new ObservableField<>(this.propertyModel.getProperty_type().getName());
        homeType = new ObservableField<>(this.propertyModel.getHome_type().getName());
        price = new ObservableField<>(Integer.toString(this.propertyModel.getPrice_dialy()));
        rate = new ObservableField<>(Integer.toString(this.propertyModel.getRate()));
        ratesNumber = new ObservableField<>("(" + this.propertyModel.getRates_number() + ")");
        city = new ObservableField<>(this.propertyModel.getCity().getName());
        area = new ObservableField<>(this.propertyModel.getArea().getName());
        trips = new ObservableField<>("(" + this.propertyModel.getStays() + ")");
    }

    public void onItemClick() {
        //mListener.onItemClick(carsModel.getBlogUrl());
    }

    public interface TopCarItemViewModelListener {

        void onItemClick(String blogUrl);
    }
}
