package g22.renzo;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import g22.renzo.mydi.application.AppComponent;
import g22.renzo.mydi.application.AppModule;
import g22.renzo.mydi.application.DaggerAppComponent;

public class RenzoApplication extends Application {


    private static RenzoApplication getApp(Context context) {
        return (RenzoApplication) context.getApplicationContext();
    }

    private final AppComponent appComponent = createAppComponent();

    private static Context context;

    public static Context getContext() {
        return context;
    }

    public static AppComponent getComponent(Context context) {
        return getApp(context).appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    private AppComponent createAppComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }
}
