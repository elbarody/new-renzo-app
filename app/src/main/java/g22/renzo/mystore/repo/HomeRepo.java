package g22.renzo.mystore.repo;


import com.google.gson.Gson;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicReference;

import g22.renzo.mydi.qualifier.ForActivity;
import g22.renzo.mydi.scope.ActivityScope;
import g22.renzo.mystore.models.home.HomeResponse;
import g22.renzo.mystore.models.home.TopCarResponse;
import g22.renzo.mystore.models.home.TopPropertyResponse;
import g22.renzo.mystore.models.more.GetAllCarsResponse;
import g22.renzo.mystore.models.more.GetAllPropertiesResponse;
import g22.renzo.mystore.network.NetworkConstants;
import g22.renzo.mystore.network.NetworkManager;
import g22.renzo.store.model.GetCarDetailsResponse;
import g22.renzo.store.model.GetFavResponse;
import g22.renzo.store.model.carModels.CarsModel;
import g22.renzo.store.network.Endpoints;
import g22.renzo.utils.PreferencesUtil;
import io.reactivex.Observable;

import static g22.renzo.mystore.repo.UserRepo.TOKEN_KEY;

@ActivityScope
public class HomeRepo {


    private final NetworkManager networkManager;
    private final PreferencesUtil preferencesUtil;
    private final Gson gson = new Gson();
    private String userToken;

    public HomeRepo(@ForActivity NetworkManager networkManager, @ForActivity PreferencesUtil preferencesUtil) {
        this.networkManager = networkManager;
        this.preferencesUtil = preferencesUtil;
        userToken = preferencesUtil.getString(TOKEN_KEY, "");
    }


    public Observable<HomeResponse> getHomeData() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", userToken);
        AtomicReference<TopPropertyResponse> topPropertyRes = new AtomicReference<>();

        return networkManager.getRequest(NetworkConstants.GET_TOP_PROPERTY, params, TopPropertyResponse.class)
                .flatMap(topPropertyResponse -> {
                    if (topPropertyResponse.getStatus() == 200) {
                        topPropertyRes.set(topPropertyResponse);
                        return Observable.just(topPropertyResponse);
                    } else return Observable.error(new Throwable("An error !!"));
                })
                .concatMap(topPropertyResponse -> networkManager.getRequest(NetworkConstants.GET_TOP_CAR, params, TopCarResponse.class))
                .flatMap(topCarResponse -> {
                    if (topCarResponse.getStatus() == 200)
                        return Observable.just(new HomeResponse(topCarResponse, topPropertyRes.get()));
                    else return Observable.error(new Throwable("An error !!"));
                });
    }

    public Observable<GetAllCarsResponse> getAllCars() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", userToken);
        return networkManager.getRequest(NetworkConstants.GET_ALL_CARS, params, GetAllCarsResponse.class)
                .flatMap(response -> {
                    if (response.getStatus() == 200) {
                        return Observable.just(response);
                    } else return Observable.error(new Throwable("An error !!"));
                });
    }

    public Observable<GetAllPropertiesResponse> getAllPrperties() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", userToken);
        return networkManager.getRequest(NetworkConstants.GET_ALL_PROPERTIES, params, GetAllPropertiesResponse.class)
                .flatMap(response -> {
                    if (response.getStatus() == 200) {
                        return Observable.just(response);
                    } else return Observable.error(new Throwable("An error !!"));
                });
    }

    public Observable<CarsModel> getCarById(int id) {
        String endPoint = NetworkConstants.GET_CAR_DETAILS + "/" + id + "/" + userToken;

        return networkManager.getRequest(endPoint, new HashMap<>(), GetCarDetailsResponse.class)
                .flatMap(resp -> {
                    if (resp.getStatus() == 200)
                        return Observable.just(resp.getCar());
                    else return Observable.error(new Throwable("An error !!"));
                });
    }

    public Observable<GetFavResponse> getFavoriteCars() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", userToken);
        return networkManager.getRequest(NetworkConstants.GET_FAVORITES_CAR, params, GetFavResponse.class)
                .flatMap(response -> {
                    if (response.getStatus() == 200)
                        return Observable.just(response);
                    else return Observable.error(new Throwable("An error !!"));
                });
    }

    public Observable<GetFavResponse> getFavoritePrperties() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", userToken);
        return networkManager.getRequest(NetworkConstants.GET_FAVORITES_PROPERITIES, params, GetFavResponse.class)
                .flatMap(response -> {
                    if (response.getStatus() == 200)
                        return Observable.just(response);
                    else return Observable.error(new Throwable("An error !!"));
                });
    }

}
