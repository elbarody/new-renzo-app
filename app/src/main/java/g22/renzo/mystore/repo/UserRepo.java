package g22.renzo.mystore.repo;


import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.hadilq.liveevent.LiveEvent;

import java.util.HashMap;

import g22.renzo.mydi.qualifier.ForActivity;
import g22.renzo.mydi.scope.ActivityScope;
import g22.renzo.mystore.models.forgetpassword.ForgetPasswordResponse;
import g22.renzo.mystore.models.register.RegistrationResponse;
import g22.renzo.mystore.models.user.UserResponse;
import g22.renzo.mystore.network.NetworkConstants;
import g22.renzo.mystore.network.NetworkManager;
import g22.renzo.store.model.loginModels.LoginResponse;
import g22.renzo.store.model.loginModels.UserData;
import g22.renzo.store.network.Endpoints;
import g22.renzo.utils.PreferencesUtil;
import g22.renzo.utils.SessionManager;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@ActivityScope
public class UserRepo {


    public static final String USER_KEY = "User_Key";
    public static final String TOKEN_KEY = "Token_Key";

    private final NetworkManager networkManager;
    private final PreferencesUtil preferencesUtil;
    private final Gson gson = new Gson();

    public UserRepo(@ForActivity NetworkManager networkManager, @ForActivity PreferencesUtil preferencesUtil) {
        this.networkManager = networkManager;
        this.preferencesUtil = preferencesUtil;
    }

    public Observable<UserResponse> login(String email, String pass) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("email", email);
        params.put("password", pass);
        //params.put("fcm_token", FirebaseInstanceId.getInstance().getToken());
        return networkManager.postRequest(NetworkConstants.LOGIN_URL, params, UserResponse.class).map(userResponse -> {
            if (userResponse.getUser() != null) {
                preferencesUtil.saveOrUpdateString(USER_KEY, gson.toJson(userResponse.getUser()));
            }
            preferencesUtil.saveOrUpdateString(TOKEN_KEY, userResponse.getToken());
            return userResponse;
        }).flatMap(userResponse -> {
            if (userResponse.getStatus() == 200)
                return Observable.just(userResponse);
            else return Observable.error(new Throwable(userResponse.getError()));
        });
    }

    public Observable<RegistrationResponse> register(String f_name, String l_name, String email, String pass) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("f_name", f_name);
        params.put("l_name", l_name);
        params.put("email", email);
        params.put("password", pass);
        //params.put("fcm_token", FirebaseInstanceId.getInstance().getToken());
        return networkManager.postRequest(NetworkConstants.REGISTER_URL, params, RegistrationResponse.class)
                .flatMap(userResponse -> {
                    if (userResponse.getStatus() == 200)
                        return Observable.just(userResponse);
                    else return Observable.error(new Throwable(userResponse.getError()));
                });
    }

    public Observable<ForgetPasswordResponse> forgetPassword(String email) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("email", email);

        return networkManager.postRequest(NetworkConstants.FORGET_PASSWORD, params, ForgetPasswordResponse.class)
                .flatMap(response -> {
                    if (response.getStatus() == 200)
                        return Observable.just(response);
                    else return Observable.error(new Throwable(response.getError()));
                });
    }

    public UserData getUserData() {

        String userJson = preferencesUtil.getString(USER_KEY, null);
        if (userJson != null)
            return gson.fromJson(userJson, UserData.class);
        else return null;

    }

}
