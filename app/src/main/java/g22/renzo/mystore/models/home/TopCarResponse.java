package g22.renzo.mystore.models.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import g22.renzo.store.model.carModels.CarsModel;

public class TopCarResponse {

    @Expose
    @SerializedName("cars")
    private List<CarsModel> cars;
    @Expose
    @SerializedName("status")
    private int status;

    public List<CarsModel> getCars() {
        return cars;
    }

    public int getStatus() {
        return status;
    }
}
