package g22.renzo.mystore.models.home;

public class HomeResponse {

    private TopCarResponse topCarResponse;
    private TopPropertyResponse topPropertyResponse;

    public HomeResponse(TopCarResponse topCarResponse, TopPropertyResponse topPropertyResponse) {
        this.topCarResponse = topCarResponse;
        this.topPropertyResponse = topPropertyResponse;
    }

    public TopCarResponse getTopCarResponse() {
        return topCarResponse;
    }

    public void setTopCarResponse(TopCarResponse topCarResponse) {
        this.topCarResponse = topCarResponse;
    }

    public TopPropertyResponse getTopPropertyResponse() {
        return topPropertyResponse;
    }

    public void setTopPropertyResponse(TopPropertyResponse topPropertyResponse) {
        this.topPropertyResponse = topPropertyResponse;
    }
}
