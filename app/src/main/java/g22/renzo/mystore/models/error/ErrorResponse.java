package g22.renzo.mystore.models.error;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ErrorResponse {

    @Expose
    @SerializedName("success")
    private boolean success;
    @Expose
    @SerializedName("status_message")
    private String statusMessage;
    @Expose
    @SerializedName("status_code")
    private int statusCode;

    public boolean isSuccess() {
        return success;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }
}
