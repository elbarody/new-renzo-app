package g22.renzo.mystore.models.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserData {
    @Expose
    @SerializedName("bank")
    private BankData bank;
    @Expose
    @SerializedName("updated_at")
    private String updated_at;
    @Expose
    @SerializedName("created_at")
    private String created_at;
    @Expose
    @SerializedName("current_language")
    private String current_language;
    @Expose
    @SerializedName("can_rate")
    private int can_rate;
    @Expose
    @SerializedName("role")
    private int role;
    @Expose
    @SerializedName("type")
    private int type;
    @Expose
    @SerializedName("national_id")
    private String national_id;
    @Expose
    @SerializedName("photo")
    private String photo;
    @Expose
    @SerializedName("verify")
    private int verify;
    @Expose
    @SerializedName("tokens")
    private String tokens;
    @Expose
    @SerializedName("email_verified_at")
    private String email_verified_at;
    @Expose
    @SerializedName("email")
    private String email;
    @Expose
    @SerializedName("l_name")
    private String l_name;
    @Expose
    @SerializedName("f_name")
    private String f_name;
    @Expose
    @SerializedName("id")
    private int id;

    public BankData getBank() {
        return bank;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getCurrent_language() {
        return current_language;
    }

    public int getCan_rate() {
        return can_rate;
    }

    public int getRole() {
        return role;
    }

    public int getType() {
        return type;
    }

    public String getNational_id() {
        return national_id;
    }

    public String getPhoto() {
        return photo;
    }

    public int getVerify() {
        return verify;
    }

    public String getTokens() {
        return tokens;
    }

    public String getEmail_verified_at() {
        return email_verified_at;
    }

    public String getEmail() {
        return email;
    }

    public String getL_name() {
        return l_name;
    }

    public String getF_name() {
        return f_name;
    }

    public int getId() {
        return id;
    }
}
