package g22.renzo.mystore.models.more;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetAllPropertiesResponse {

    @Expose
    @SerializedName("properties")
    private PropertiesResponse properties;
    @Expose
    @SerializedName("status")
    private int status;

    public PropertiesResponse getProperties() {
        return properties;
    }

    public int getStatus() {
        return status;
    }
}
