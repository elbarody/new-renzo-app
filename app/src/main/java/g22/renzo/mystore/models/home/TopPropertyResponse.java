package g22.renzo.mystore.models.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import g22.renzo.store.model.property.PropertiesModel;

public class TopPropertyResponse {

    @Expose
    @SerializedName("properties")
    private List<PropertiesModel> properties;
    @Expose
    @SerializedName("status")
    private int status;

    public List<PropertiesModel> getProperties() {
        return properties;
    }

    public int getStatus() {
        return status;
    }
}
