package g22.renzo.mystore.models.more;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetAllCarsResponse {

    @Expose
    @SerializedName("cars")
    private CarsRsponse cars;
    @Expose
    @SerializedName("status")
    private int status;

    public CarsRsponse getCars() {
        return cars;
    }

    public int getStatus() {
        return status;
    }
}
