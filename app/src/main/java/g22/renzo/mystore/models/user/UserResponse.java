package g22.renzo.mystore.models.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserResponse {

    @Expose
    @SerializedName("user")
    private UserData user;
    @Expose
    @SerializedName("token")
    private String token;
    @Expose
    @SerializedName("status")
    private int status;

    @Expose
    @SerializedName("error")
    private String error;

    public UserData getUser() {
        return user;
    }

    public String getToken() {
        return token;
    }

    public String getError() {
        return error;
    }

    public int getStatus() {
        return status;
    }
}
