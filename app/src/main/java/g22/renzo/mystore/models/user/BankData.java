package g22.renzo.mystore.models.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BankData {
    @Expose
    @SerializedName("updated_at")
    private String updated_at;
    @Expose
    @SerializedName("created_at")
    private String created_at;
    @Expose
    @SerializedName("user_id")
    private int user_id;
    @Expose
    @SerializedName("branch_address")
    private String branch_address;
    @Expose
    @SerializedName("swift_code")
    private String swift_code;
    @Expose
    @SerializedName("account_number")
    private String account_number;
    @Expose
    @SerializedName("bank_name")
    private String bank_name;
    @Expose
    @SerializedName("id")
    private int id;

    public String getUpdated_at() {
        return updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getBranch_address() {
        return branch_address;
    }

    public String getSwift_code() {
        return swift_code;
    }

    public String getAccount_number() {
        return account_number;
    }

    public String getBank_name() {
        return bank_name;
    }

    public int getId() {
        return id;
    }
}
