package g22.renzo.mystore.models.language;

public class LanguageModel {

    private String id;
    private String name;
    private String code;

    public LanguageModel(String id, String name,String code) {
        this.id = id;
        this.name = name;
        this.code = code;

    }

    public String getCode() {
        return code;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
