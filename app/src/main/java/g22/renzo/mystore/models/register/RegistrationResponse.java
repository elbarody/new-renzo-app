package g22.renzo.mystore.models.register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegistrationResponse {

    @Expose
    @SerializedName("error")
    private String error;
    @Expose
    @SerializedName("success")
    private String success;
    @Expose
    @SerializedName("status")
    private int status;

    public String getError() {
        return error;
    }

    public String getSuccess() {
        return success;
    }

    public int getStatus() {
        return status;
    }


    }

