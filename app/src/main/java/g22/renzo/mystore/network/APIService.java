package g22.renzo.mystore.network;

import com.google.gson.JsonElement;

import java.util.List;
import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface APIService {

    @GET
    Observable<JsonElement> getRequest(@Url String api, @HeaderMap Map<String, String> headers, @QueryMap Map<String, Object> param);


//    @GET

    //    Observable<JsonElement> getPathRequest(@Url String api, @HeaderMap Map<String, String> headers, @Path("searchCriteria[filter_groups][0][filters][0][field]") String par1);

    @Multipart
    @POST
    Observable<JsonElement> multipartRequest(@Url String api, @HeaderMap Map<String, String> headers,
                                             @PartMap/*(encoding = "utf-8")*/ Map<String, RequestBody> data,
                                             @Part List<MultipartBody.Part> attachments);


    @PUT
    Observable<JsonElement> putRequest(@Url String api, @HeaderMap Map<String, String> headers, @Body Map<String, Object> body);


    @FormUrlEncoded
    @PUT
    Observable<JsonElement> putFieldRequest(@Url String api, @HeaderMap Map<String, String> headers, @FieldMap(encoded = true) Map<String, Object> body);


    @POST
    Observable<JsonElement> postRequest(@Url String api, @HeaderMap Map<String, String> headers, @Body Object body);


//    @Multipart
//    @POST
//    Observable<JsonElement> multipartRequest(@Url String api, @HeaderMap Map<String, String> headers, @PartMap Map<String, RequestBody> data,
//
//                                             @PartMap Map<String, MultipartBody.Part> files);
//

    @PUT
    Completable completablePutRequest(@Url String api, @HeaderMap Map<String, String> headers, @Body Map<String, Object> body);


    @POST
    Completable completablePostRequest(@Url String api, @HeaderMap Map<String, String> headers, @Body Map<String, Object> body);


    @DELETE
    Completable completableDeleteRequest(@Url String api, @HeaderMap Map<String, String> headers);


    @DELETE
    Observable<JsonElement> deleteRequest(@Url String api, @HeaderMap Map<String, String> headers);
}
