package g22.renzo.mystore.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import g22.renzo.mydi.qualifier.ForActivity;
import g22.renzo.mydi.scope.ActivityScope;
import io.reactivex.Completable;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

@ActivityScope
public class NetworkManager {

    private final static Map<String, String> headers = new HashMap<>();

    private final APIService apiService;


    @Inject
    public NetworkManager(@ForActivity APIService apiService) {
        this.apiService = apiService;
    }

    public void addAuthorization(String token) {

        if (token != null)

            headers.put("Authorization", "Bearer ".concat(token));

    }

    public void removeAuthorization() {

        headers.remove("Authorization");

    }



    public <T> Observable<T> getRequest(String api, Map<String, Object> param, Class<T> parseClass) {

        if (param == null)

            param = new HashMap<>();


        return apiService.getRequest(api, headers, param).map(jsonElement -> {

            Gson gson = new GsonBuilder().serializeNulls().create();

            return gson.fromJson(jsonElement, parseClass);

        });

    }

    public <T> Observable<T> multiPartRequest(String api, Map<String, RequestBody> param, List<MultipartBody.Part> files, Class<T> parseClass) {
        if (param == null)
            param = new HashMap<>();
        if (files == null)
            files = new ArrayList<>();

        return apiService.multipartRequest(api, headers, param, files).map(jsonElement -> {
            Gson gson = new GsonBuilder().serializeNulls().create();
            return gson.fromJson(jsonElement, parseClass);
        });
    }

    public <T> Observable<List<T>> postListRequest(String api, Map<String, Object> param, Class<T> parseClass) {

        if (param == null)

            param = new HashMap<>();


        return apiService.postRequest(api, headers, param).map(jsonElement -> {

            Gson gson = new GsonBuilder().serializeNulls().create();

            Type collectionType = TypeToken.getParameterized(List.class, parseClass).getType();

            return gson.fromJson(jsonElement, collectionType);

        });

    }


    public <T> Observable<List<T>> getListRequest(String api, Map<String, Object> param, Class<T> parseClass) {

        if (param == null)

            param = new HashMap<>();


        return apiService.getRequest(api, headers, param).map(jsonElement -> {

            Gson gson = new GsonBuilder().serializeNulls().create();

            Type collectionType = TypeToken.getParameterized(List.class, parseClass).getType();

            return gson.fromJson(jsonElement, collectionType);

        });

    }


    public <T> Observable<T> postRequest(String api, Map<String, Object> param, Class<T> parseClass) {

        if (param == null)

            param = new HashMap<>();

        return apiService.postRequest(api, headers, param).map(jsonElement -> {
            Gson gson = new GsonBuilder().serializeNulls().create();
            return gson.fromJson(jsonElement, parseClass);

        });


    }


    public Completable completablePostRequest(String api, Map<String, Object> param) {

        if (param == null)
            param = new HashMap<>();

        return apiService.completablePostRequest(api, headers, param);
    }


    public Completable completableDeleteRequest(String api) {
        return apiService.completableDeleteRequest(api, headers);
    }


    public <T> Observable<T> putRequest(String api, Map<String, Object> param, Class<T> parseClass) {

        if (param == null)
            param = new HashMap<>();

        return apiService.putRequest(api, headers, param).map(jsonElement -> {

            Gson gson = new GsonBuilder().serializeNulls().create();

            return gson.fromJson(jsonElement, parseClass);

        });

    }

    public <T> Observable<T> deleteRequest(String api, Map<String, Object> param, Class<T> parseClass) {

        if (param == null)

            param = new HashMap<>();


        return apiService.deleteRequest(api, headers)

                .map(jsonElement -> {

                    Gson gson = new GsonBuilder().serializeNulls().create();

                    return gson.fromJson(jsonElement, parseClass);

                });

    }


    public <T> Observable<T> putFieldRequest(String api, Map<String, Object> param, Class<T> parseClass) {

        if (param == null)

            param = new HashMap<>();


        return apiService.putFieldRequest(api, headers, param).map(jsonElement -> {

            Gson gson = new GsonBuilder().serializeNulls().create();

            return gson.fromJson(jsonElement, parseClass);

        });


    }

}
