package g22.renzo.di.builder;


import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import g22.renzo.ui.favorites.FavoritesActivity;
import g22.renzo.ui.forgetPassword.ForgetPasswordActivity;
import g22.renzo.ui.login.LoginActivity;
import g22.renzo.ui.registration.RegistrationActivity;


@Module
public abstract class ActivityBuilder {

//    @ContributesAndroidInjector
//    abstract SplashActivity bindSplashActivity();

    @ContributesAndroidInjector
    abstract LoginActivity bindLoginActivity();

    @ContributesAndroidInjector
    abstract RegistrationActivity bindRegistrationActivity();

    @ContributesAndroidInjector
    abstract ForgetPasswordActivity bindForgetPasswordActivity();
//
//    @ContributesAndroidInjector(modules = {MainActivityModule.class})
//    abstract MainActivity bindMainActivity();

//    @ContributesAndroidInjector(modules = {MoreCarsActivityModule.class})
//    abstract MoreActivity bindMoreCarsActivity();

//    @ContributesAndroidInjector(modules = {MorePropertiesActivityModule.class})
//    abstract MorePropertiesActivity bindMorePropertiesActivity();
//
//    @ContributesAndroidInjector(modules = {
//            CarsDetailsActivityModule.class})
//    abstract CarDetailsActivity bindCarDetailsActivity();

//    @ContributesAndroidInjector(modules = {
//            FavoritesActivityModule.class})
//    abstract FavoritesActivity bindFavoritesActivity();
}
