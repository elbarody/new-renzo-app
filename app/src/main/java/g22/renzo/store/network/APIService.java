package g22.renzo.store.network;

import com.google.gson.JsonElement;

import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface APIService {

    @GET
    Observable<JsonElement> getRequest(@Url String api, @HeaderMap Map<String, Object> headers, @QueryMap Map<String, Object> param);

    @GET
    Observable<JsonElement> getRequest(@Url String api, @HeaderMap Map<String, Object> headers);
//    @GET
//    Observable<JsonElement> getPathRequest(@Url String api, @HeaderMap Map<String, String> headers, @Path("searchCriteria[filter_groups][0][filters][0][field]") String par1);

    @PUT
    Observable<JsonElement> putRequest(@Url String api, @HeaderMap Map<String, Object> headers, @Body Map<String, Object> body);

    @PUT
    Observable<JsonElement> putFieldRequest(@Url String api, @HeaderMap Map<String, Object> headers, @FieldMap Map<String, Object> body);

    @FormUrlEncoded
    @POST
    Observable<JsonElement> postRequest(@Url String api, @HeaderMap Map<String, Object> headers, @FieldMap Map<String, Object> body);

    @Multipart
    @POST
    Observable<JsonElement> multipartRequest(@Url String api, @HeaderMap Map<String, Object> headers, @PartMap Map<String, RequestBody> data,
                                             @Part MultipartBody.Part[] attachments);

    @Multipart
    @POST
    Observable<JsonElement> multipartRequestJustFile(@Url String api, @HeaderMap Map<String, Object> headers, @PartMap Map<String, RequestBody> data,
                                                     @Part MultipartBody.Part img);

    @PUT
    Completable completablePutRequest(@Url String api, @HeaderMap Map<String, Object> headers, @Body Map<String, Object> body);

    @POST
    Completable completablePostRequest(@Url String api, @HeaderMap Map<String, Object> headers, @Body Map<String, Object> body);
}