package g22.renzo.store.network;

public class Endpoints {

    public static final String BASE_URL = "http://157.230.117.74/api/";
    public static final String BASE_URL_IMAGE = "http://157.230.117.74";

    public static final String LOGIN_URL = "login";
    public static final String REGISTER_URL = "register";
    public static final String FORGET_PASSWORD = "getResetToken";
    public static final String GET_TOP_CAR = "getTopCars";
    public static final String GET_TOP_PROPERTY = "getTopProperties";
    public static final String GET_ALL_CARS = "getCars";
    public static final String GET_ALL_PROPERTIES = "getProperties";
    public static final String GET_CAR_DETAILS = "getCarDetails";
    public static final String GET_FAVORITES_CAR = "myFavsCarList";
    public static final String GET_FAVORITES_PROPERITIES = "myFavsPropertyList";

}
