package g22.renzo.store.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class NetworkManager {

    private final static Map<String, Object> headers = new HashMap<String, Object>() {{
        put("Accept", "application/json");
        //put("Content-Type", "application/x-www-form-urlencoded");
    }};

    public <T> Observable<T> getRequest(String api, Map<String, Object> param, Class<T> parseClass) {
        if (param == null)
            param = new HashMap<>();

        return NetworkDispatcher.getRetrofit().create(APIService.class)
                .getRequest(api, headers, param).map(jsonElement -> {
                    Gson gson = new GsonBuilder().serializeNulls().create();
                    return gson.fromJson(jsonElement, parseClass);
                });
    }

    public <T> Observable<T> getRequest(String api, Class<T> parseClass) {

        return NetworkDispatcher.getRetrofit().create(APIService.class)
                .getRequest(api, headers).map(jsonElement -> {
                    Gson gson = new GsonBuilder().serializeNulls().create();
                    return gson.fromJson(jsonElement, parseClass);
                });
    }

    public <T> Observable<T> postRequest(String api, Map<String, Object> param, Class<T> parseClass) {
        if (param == null)
            param = new HashMap<>();
        return NetworkDispatcher.getRetrofit().create(APIService.class)
                .postRequest(api, headers, param).map(jsonElement -> {
                    Gson gson = new GsonBuilder().serializeNulls().create();
                    return gson.fromJson(jsonElement, parseClass);
                });
    }

    public <T> Observable<T> multiPartRequest(String api, Map<String, RequestBody> param, List<MultipartBody.Part> files, Class<T> parseClass) {
        if (param == null)
            param = new HashMap<>();
        if (files == null)
            files = new ArrayList<>();

        return NetworkDispatcher.getRetrofit().create(APIService.class)
                .multipartRequest(api, headers, param, files.toArray(new MultipartBody.Part[files.size()])).map(jsonElement -> {
                    Gson gson = new GsonBuilder().serializeNulls().create();
                    return gson.fromJson(jsonElement, parseClass);
                });
    }

    public <T> Observable<T> multiPartRequestJustFile(String api, Map<String, RequestBody> param, MultipartBody.Part files, Class<T> parseClass) {
        if (param == null)
            param = new HashMap<>();

        return NetworkDispatcher.getRetrofit().create(APIService.class)
                .multipartRequestJustFile(api, headers, param, files).map(jsonElement -> {
                    Gson gson = new GsonBuilder().serializeNulls().create();
                    return gson.fromJson(jsonElement, parseClass);
                });
    }
}
