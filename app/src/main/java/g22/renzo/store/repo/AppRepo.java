package g22.renzo.store.repo;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import g22.renzo.base.BaseView;
import g22.renzo.mystore.models.more.GetAllCarsResponse;
import g22.renzo.mystore.models.more.GetAllPropertiesResponse;
import g22.renzo.store.model.GetCarDetailsResponse;
import g22.renzo.mystore.models.home.TopCarResponse;
import g22.renzo.store.model.GetFavResponse;
import g22.renzo.mystore.models.home.TopPropertyResponse;
import g22.renzo.store.model.carModels.CarsModel;
import g22.renzo.store.model.loginModels.LoginResponse;
import g22.renzo.store.model.RegistrationResponse;
import g22.renzo.store.model.loginModels.UserData;
import g22.renzo.store.model.property.PropertiesModel;
import g22.renzo.store.network.Endpoints;
import g22.renzo.store.network.NetworkManager;
import g22.renzo.utils.SessionManager;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class AppRepo {

    public MutableLiveData<Boolean> isActionSuccess = new MutableLiveData<>();
    public MutableLiveData<LoginResponse> loginLiveDataMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<RegistrationResponse> registrationLiveDataMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<List<CarsModel>> topCarLiveDataMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<List<PropertiesModel>> topPropertyMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<GetCarDetailsResponse> carDetailsMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<GetFavResponse> favResponseMutableLiveData = new MutableLiveData<>();


    private NetworkManager networkManager;
    private CompositeDisposable disposable;
    private BaseView view;
    private SessionManager sessionManager;
    private UserData user = new UserData();
    private Context context;


    @Inject
    AppRepo(Context context) {
        this.context = context;
        this.view = (BaseView) context;
        this.disposable = new CompositeDisposable();
        this.networkManager = new NetworkManager();
        //this.sessionManager = SessionManager.class;
        /*user=new UserData();
        user.setName("ahmed");
        SessionManager.createLoginSession(user);*/
    }


    public void login(String email, String pass) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("email", email);
        params.put("password", pass);
        //params.put("fcm_token", FirebaseInstanceId.getInstance().getToken());

        disposable.add(networkManager.postRequest(Endpoints.LOGIN_URL, params, LoginResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    loginLiveDataMutableLiveData.setValue(response);
                    SessionManager.createSession(context,response);
                }, this::processError));
    }

    //
    public void getTopCar(String token) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", token);
        disposable.add(networkManager.getRequest(Endpoints.GET_TOP_CAR, params, TopCarResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    topCarLiveDataMutableLiveData.setValue(response.getCars());
                }, this::processError));
    }

    public void getTopProperty(String token) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", token);
        disposable.add(networkManager.getRequest(Endpoints.GET_TOP_PROPERTY, params, TopPropertyResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    topPropertyMutableLiveData.setValue(response.getProperties());
                }, this::processError));
    }

    public void getAllCars(String token) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", token);
        disposable.add(networkManager.getRequest(Endpoints.GET_ALL_CARS, params, GetAllCarsResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    topCarLiveDataMutableLiveData.setValue(response.getCars().getData());
                }, this::processError));
    }

    public void getAllPrperties(String token) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", token);
        disposable.add(networkManager.getRequest(Endpoints.GET_ALL_PROPERTIES, params, GetAllPropertiesResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    topPropertyMutableLiveData.setValue(response.getProperties().getData());
                }, this::processError));
    }


    public void getFavorites(String token, String type) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", token);
        String endPoint;
        if (type.equals("car"))
            endPoint = Endpoints.GET_FAVORITES_CAR;
        else
            endPoint = Endpoints.GET_FAVORITES_PROPERITIES;
        disposable.add(networkManager.getRequest(endPoint, params, GetFavResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    favResponseMutableLiveData.setValue(response);
                }, this::processError));
    }

    public void getCarDetails(int id, String token) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", token);
        params.put("id", id);
        String endPoint = Endpoints.GET_CAR_DETAILS + "/" + id + "/" + token;
        disposable.add(networkManager.getRequest(endPoint, GetCarDetailsResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    carDetailsMutableLiveData.setValue(response);
                }, this::processError));
    }


    public void forgetPassword(String email) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("email", email);

        disposable.add(networkManager.postRequest(Endpoints.FORGET_PASSWORD, params, RegistrationResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    registrationLiveDataMutableLiveData.setValue(response);
                }, this::processError));
    }

    public void registration(String f_name, String l_name, String email, String pass) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("f_name", f_name);
        params.put("l_name", l_name);
        params.put("email", email);
        params.put("password", pass);

        disposable.add(networkManager.postRequest(Endpoints.REGISTER_URL, params, RegistrationResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    registrationLiveDataMutableLiveData.setValue(response);
                }, this::processError));
    }


    private void processError(Throwable throwable) {
        view.hideLoading();
        if (throwable instanceof IOException)
            view.showErrorMessage("No internet connection !!");
        else
            view.showErrorMessage("An error ");
    }

    public void dispose() {
        disposable.dispose();
    }


}



