package g22.renzo.store.model.property;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PropertyTypeModel {
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("id")
    private int id;

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}
