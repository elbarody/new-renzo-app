package g22.renzo.store.model.carModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import g22.renzo.store.model.areasModel.AreaModel;
import g22.renzo.store.model.areasModel.CityModel;
import g22.renzo.store.model.loginModels.UserData;

public class CarsModel {
    @Expose
    @SerializedName("images")
    private List<ImagesModel> images;
    @Expose
    @SerializedName("user")
    private UserData user;
    @Expose
    @SerializedName("mileage")
    private MileageModel mileage;
    @Expose
    @SerializedName("transimission_type")
    private TransimissionTypeModel transimission_type;
    @Expose
    @SerializedName("aminities")
    private List<AminitiesModel> aminities;
    @Expose
    @SerializedName("city")
    private CityModel city;
    @Expose
    @SerializedName("area")
    private AreaModel area;
    @Expose
    @SerializedName("model")
    private ModelEntity model;
    @Expose
    @SerializedName("make")
    private MakeEntity make;
    @Expose
    @SerializedName("fav")
    private boolean fav;
    @Expose
    @SerializedName("rates_number")
    private int rates_number;
    @Expose
    @SerializedName("rate")
    private int rate;
    @Expose
    @SerializedName("updated_at")
    private String updated_at;
    @Expose
    @SerializedName("created_at")
    private String created_at;
    @Expose
    @SerializedName("booking_type")
    private String booking_type;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("number_of_views")
    private int number_of_views;
    @Expose
    @SerializedName("price")
    private int price;
    @Expose
    @SerializedName("trips")
    private int trips;
    @Expose
    @SerializedName("description")
    private String description;
    @Expose
    @SerializedName("longitude")
    private String longitude;
    @Expose
    @SerializedName("latitude")
    private String latitude;
    @Expose
    @SerializedName("engine_cc")
    private String engine_cc;
    @Expose
    @SerializedName("color")
    private String color;
    @Expose
    @SerializedName("year")
    private String year;
    @Expose
    @SerializedName("mileage_id")
    private int mileage_id;
    @Expose
    @SerializedName("transmission_type_id")
    private int transmission_type_id;
    @Expose
    @SerializedName("city_id")
    private int city_id;
    @Expose
    @SerializedName("area_id")
    private int area_id;
    @Expose
    @SerializedName("make_id")
    private int make_id;
    @Expose
    @SerializedName("model_id")
    private int model_id;
    @Expose
    @SerializedName("user_id")
    private int user_id;
    @Expose
    @SerializedName("id")
    private int id;

    public List<AminitiesModel> getAminities() {
        return aminities;
    }

    public int getTrips() {
        return trips;
    }

    public boolean isFav() {
        return fav;
    }

    public List<ImagesModel> getImages() {
        return images;
    }

    public UserData getUser() {
        return user;
    }

    public MileageModel getMileage() {
        return mileage;
    }

    public TransimissionTypeModel getTransimission_type() {
        return transimission_type;
    }

    public CityModel getCity() {
        return city;
    }

    public AreaModel getArea() {
        return area;
    }

    public ModelEntity getModel() {
        return model;
    }

    public MakeEntity getMake() {
        return make;
    }

    public boolean getFav() {
        return fav;
    }

    public int getRates_number() {
        return rates_number;
    }

    public int getRate() {
        return rate;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getBooking_type() {
        return booking_type;
    }

    public String getStatus() {
        return status;
    }

    public int getNumber_of_views() {
        return number_of_views;
    }

    public int getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getEngine_cc() {
        return engine_cc;
    }

    public String getColor() {
        return color;
    }

    public String getYear() {
        return year;
    }

    public int getMileage_id() {
        return mileage_id;
    }

    public int getTransmission_type_id() {
        return transmission_type_id;
    }

    public int getCity_id() {
        return city_id;
    }

    public int getArea_id() {
        return area_id;
    }

    public int getMake_id() {
        return make_id;
    }

    public int getModel_id() {
        return model_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public int getId() {
        return id;
    }

    public static class ImagesModel {
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("car_id")
        private int car_id;
        @Expose
        @SerializedName("id")
        private int id;

        public String getImage() {
            return image;
        }

        public int getCar_id() {
            return car_id;
        }

        public int getId() {
            return id;
        }
    }
}
