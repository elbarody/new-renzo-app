package g22.renzo.store.model.carModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransimissionTypeModel {
    @Expose
    @SerializedName("type")
    private String type;
    @Expose
    @SerializedName("id")
    private int id;

    public String getType() {
        return type;
    }

    public int getId() {
        return id;
    }
}
