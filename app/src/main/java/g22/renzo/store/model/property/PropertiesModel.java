package g22.renzo.store.model.property;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import g22.renzo.store.model.areasModel.AreaModel;
import g22.renzo.store.model.areasModel.CityModel;
import g22.renzo.store.model.loginModels.UserData;

public class PropertiesModel {
    @Expose
    @SerializedName("images")
    private List<Images> images;
    @Expose
    @SerializedName("user")
    private UserData user;
    @Expose
    @SerializedName("city")
    private CityModel city;
    @Expose
    @SerializedName("area")
    private AreaModel area;
    @Expose
    @SerializedName("home_type")
    private HomeTypeModel home_type;
    @Expose
    @SerializedName("property_type")
    private PropertyTypeModel property_type;
    @Expose
    @SerializedName("fav")
    private boolean fav;
    @Expose
    @SerializedName("rates_number")
    private int rates_number;
    @Expose
    @SerializedName("rate")
    private int rate;
    @Expose
    @SerializedName("updated_at")
    private String updated_at;
    @Expose
    @SerializedName("created_at")
    private String created_at;
    @Expose
    @SerializedName("additional_rules")
    private String additional_rules;
    @Expose
    @SerializedName("total_beds")
    private int total_beds;
    @Expose
    @SerializedName("total_guest")
    private int total_guest;
    @Expose
    @SerializedName("total_bathrooms")
    private int total_bathrooms;
    @Expose
    @SerializedName("total_bedrooms")
    private int total_bedrooms;
    @Expose
    @SerializedName("number_of_views")
    private int number_of_views;
    @Expose
    @SerializedName("price_dialy")
    private int price_dialy;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("description")
    private String description;
    @Expose
    @SerializedName("booking_type")
    private String booking_type;
    @Expose
    @SerializedName("longitude")
    private String longitude;
    @Expose
    @SerializedName("latitude")
    private String latitude;
    @Expose
    @SerializedName("city_id")
    private int city_id;
    @Expose
    @SerializedName("area_id")
    private int area_id;
    @Expose
    @SerializedName("user_id")
    private int user_id;
    @Expose
    @SerializedName("home_type_id")
    private int home_type_id;
    @Expose
    @SerializedName("stays")
    private int stays;

    @Expose
    @SerializedName("property_type_id")
    private int property_type_id;
    @Expose
    @SerializedName("id")
    private int id;

    public int getStays() {
        return stays;
    }

    public boolean isFav() {
        return fav;
    }

    public List<Images> getImages() {
        return images;
    }

    public UserData getUser() {
        return user;
    }

    public CityModel getCity() {
        return city;
    }

    public AreaModel getArea() {
        return area;
    }

    public HomeTypeModel getHome_type() {
        return home_type;
    }

    public PropertyTypeModel getProperty_type() {
        return property_type;
    }

    public boolean getFav() {
        return fav;
    }

    public int getRates_number() {
        return rates_number;
    }

    public int getRate() {
        return rate;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getAdditional_rules() {
        return additional_rules;
    }

    public int getTotal_beds() {
        return total_beds;
    }

    public int getTotal_guest() {
        return total_guest;
    }

    public int getTotal_bathrooms() {
        return total_bathrooms;
    }

    public int getTotal_bedrooms() {
        return total_bedrooms;
    }

    public int getNumber_of_views() {
        return number_of_views;
    }

    public int getPrice_dialy() {
        return price_dialy;
    }

    public String getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }

    public String getBooking_type() {
        return booking_type;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public int getCity_id() {
        return city_id;
    }

    public int getArea_id() {
        return area_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public int getHome_type_id() {
        return home_type_id;
    }

    public int getProperty_type_id() {
        return property_type_id;
    }

    public int getId() {
        return id;
    }

    public static class Images {
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("property_id")
        private int property_id;
        @Expose
        @SerializedName("id")
        private int id;

        public String getImage() {
            return image;
        }

        public int getProperty_id() {
            return property_id;
        }

        public int getId() {
            return id;
        }
    }
}
