package g22.renzo.store.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import g22.renzo.store.model.carModels.CarsModel;

public class GetCarDetailsResponse {

    @Expose
    @SerializedName("car")
    private CarsModel car;
    @Expose
    @SerializedName("status")
    private int status;

    public CarsModel getCar() {
        return car;
    }

    public int getStatus() {
        return status;
    }
}
