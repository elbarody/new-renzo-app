package g22.renzo.store.model.carModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MileageModel {
    @Expose
    @SerializedName("mileage")
    private String mileage;
    @Expose
    @SerializedName("id")
    private int id;

    public String getMileage() {
        return mileage;
    }

    public int getId() {
        return id;
    }
}
