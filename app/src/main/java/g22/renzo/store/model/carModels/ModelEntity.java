package g22.renzo.store.model.carModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelEntity {
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("id")
    private int id;

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}
