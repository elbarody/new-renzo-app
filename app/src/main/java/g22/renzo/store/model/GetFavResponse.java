package g22.renzo.store.model;

import androidx.lifecycle.LiveData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetFavResponse {
    @Expose
    @SerializedName("favs")
    private FavsData favs;
    @Expose
    @SerializedName("status")
    private int status;

    public FavsData getFavs() {
        return favs;
    }

    public int getStatus() {
        return status;
    }
}
