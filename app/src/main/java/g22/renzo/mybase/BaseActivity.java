package g22.renzo.mybase;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.ParameterizedType;

import javax.inject.Inject;

import g22.renzo.R;
import g22.renzo.mydi.activity.ActivityComponent;
import g22.renzo.utils.PreferencesUtil;

abstract public class BaseActivity<P extends BaseViewModel, V extends ViewDataBinding> extends AppCompatActivity {

    @Inject
    ViewModelFactory viewModelFactory;
    @Inject
    PreferencesUtil preferencesUtil;

    public P viewModel;
    public V dataBindingView;
    public ActivityComponent component;

    private Dialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initActivityComponent();
        if (component != null && getViewModelClass() != null) {
            viewModel = ViewModelProviders.of(this, viewModelFactory).get(getViewModelClass());
        }
        dataBindingView = DataBindingUtil.setContentView(this, getLayoutId());
        dataBindingView.setLifecycleOwner(this);
        initProgressDialog();
        initActivity();
        initObservers();
    }

    protected abstract Class<P> getViewModelClass();

    protected abstract void initActivityComponent();

    protected abstract int getLayoutId();

    protected abstract void initActivity();

    public void initObservers() {

        if (viewModel != null) {
            viewModel.errorMsgLiveData.observe(this, this::showErrorMsg);

            viewModel.successMsgLiveData.observe(this, this::showSuccessMsg);

            viewModel.loadingLiveData.observe(this, loading -> {
                if (loading)
                    progressDialog.show();
                else
                    progressDialog.dismiss();
            });
        }
    }

    private void initProgressDialog() {
        progressDialog = new Dialog(this);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setContentView(R.layout.custom_dialog_upload);
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public void showSuccessMsg(String msg) {
        if (msg != null)
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public void showErrorMsg(String msg) {
        if (msg != null)
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
