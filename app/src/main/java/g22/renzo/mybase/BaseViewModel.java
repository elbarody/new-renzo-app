package g22.renzo.mybase;

import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.hadilq.liveevent.LiveEvent;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;

import java.io.IOException;

import g22.renzo.mystore.models.error.ErrorResponse;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BaseViewModel extends ViewModel {

    private final CompositeDisposable disposable = new CompositeDisposable();
    public LiveEvent<String> errorMsgLiveData = new LiveEvent<>();
    public LiveEvent<String> successMsgLiveData = new LiveEvent<>();
    public LiveEvent<Boolean> loadingLiveData = new LiveEvent<>();
    public LiveEvent<Boolean> finishActivityLiveData = new LiveEvent<>();

    public void processError(Throwable throwable) {
        loadingLiveData.setValue(false);
        if (throwable instanceof HttpException) {
            if (((HttpException) throwable).code() == 401) {
                errorMsgLiveData.setValue(getHttpErrorMessage((HttpException) throwable));
            }else {
                errorMsgLiveData.setValue(getHttpErrorMessage((HttpException) throwable));
            }
        } else if (throwable instanceof IOException)
            errorMsgLiveData.setValue("تاكد من الاتصال بالانترنت");
        else {
            errorMsgLiveData.setValue(throwable.getLocalizedMessage());
        }
    }

    private String getHttpErrorMessage(HttpException throwable) {
        try {
            ErrorResponse errorResponse = new Gson().fromJson(throwable.response().errorBody().string(), ErrorResponse.class);
            if (errorResponse.getStatusMessage() != null) {
                return errorResponse.getStatusMessage();
            } else {
                return "An error occur try again later.";
            }
        } catch (Exception e) {
            return "An error occur try again later.";
        }
    }


    public void addToDisposable(Disposable newDisposable){
        disposable.remove(newDisposable);
        disposable.add(newDisposable);
    }

    @Override
    protected void onCleared() {
        disposable.clear();
        super.onCleared();
    }
}
